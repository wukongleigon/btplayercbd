package com.hdx.btlive.constants;

public class Constants {
	public static boolean isSetBtscg = true;
	public static boolean isSetTk = false;
	public static String WHICH_LINE = "MyFinder";

	public static final int TKCN = 0x001;
	public static final int SCJY = 0x002;
	public static final int TK = 0x003;
	public static final int BTDIGG =0x004;

	public static final int BTSCG_OK = 0x005;
	public static final int TKCN_OK = 0x006;
	public static final int TK_OK = 0x007;
	
	public static final int MESSAGE_ERROR = 0x008;
	public static final int WIFI_CLOSE = 0x009;
	public static final int IS_MAGNET_OR_NOT = 0x010;
	public static final int CANCLE_SCAN_TORRENT_TASK = 0x011;
	public static final int CANCLE_PARSE_URI_TASK = 0x012;
	public static final int CANCLE_AND_INTENT_HOME = 0x013;
	public static final int CANCLE_GET_PLAYABLE_URL = 0x014;
	
	public static final String RESPONSE_MESSAGE_ERROR = "response_error_message";

	public static boolean BT_RESULT_OK = false;
	public static boolean TK_RESULT_OK = false;
	public static boolean TKCN_RESULT_OK=false;
	public static boolean BTDIGG_RESULT_OK=false;

	public static boolean GET_BTSCG_LIST = false;
	public static boolean GET_TKCN_LIST = false;
	
	public static boolean isInExploreTask =false;

}
