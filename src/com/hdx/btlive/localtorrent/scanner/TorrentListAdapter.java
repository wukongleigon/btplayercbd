package com.hdx.btlive.localtorrent.scanner;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hdx.btlive.R;

public class TorrentListAdapter extends BaseAdapter {
	Context mContext;
	List<File> list ;
	String[] filesToString;
  
	
	public TorrentListAdapter(Context context, List<File> list) {
		super();
		this.mContext = context;
		this.filesToString = FileToStr(list);
		this.list = list;
	}
   
	
	
	public String[] getFilesToString() {
		return filesToString;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder = null;
		if (null == convertView) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.image_text_item, parent, false);
			holder = new ViewHolder();
			convertView.setTag(holder);
			holder.tvName = (TextView) convertView.findViewById(R.id.label);
			holder.imgTitle = (ImageView)convertView.findViewById(R.id.image);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		if(list.size() != 0){
			holder.tvName.setText(list.get(position).getName());
			holder.imgTitle.setImageResource(R.drawable.scan_customize_icon);
		}
		return convertView;
	}
	  
	public String[] FileToStr(List<File> f) {
		// f = LocalTorrentFragment.fileList
		ArrayList<String> listStr = new ArrayList<String>();
		// set ? 不重复？ Google
		Set<String> set = new HashSet<String>();

		for (int i = 0; i < f.size(); i++) {
			String nameString = f.get(i).getName();
			set.add(nameString);
		}
		Log.v("LIST_SIZE", String.valueOf(listStr.size()));
		return set.toArray(new String[0]);
	}
	
	public final class ViewHolder {
		TextView tvName;
		ImageView imgTitle;
	}

}
