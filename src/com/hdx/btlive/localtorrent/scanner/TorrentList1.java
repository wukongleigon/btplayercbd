package com.hdx.btlive.localtorrent.scanner;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.hdx.btlive.R;
import com.hdx.btlive.constants.Constants;
import com.hdx.btlive.finder.BaseFinder;
import com.hdx.btlive.finder.MyFinder;
import com.hdx.btlive.localtorrent.torrentparse.BTihCalculator;
import com.hdx.btlive.player.VideoViewPlayingActivity;
import com.hdx.btlive.sniffer.BaseSniffer.BTXunleiItem;
import com.hdx.btlive.sniffer.BaseSniffer.BTihItem;
import com.hdx.btlive.sniffer.BaseSniffer.VideoItem;
import com.hdx.btlive.ui.LocalTorrentFragment;
import com.hdx.btlive.ui.selfdialog.CustomProgressDialog;
import com.umeng.analytics.MobclickAgent;

public class TorrentList1 extends Activity {

	BaseFinder mFinder = new MyFinder();
	CustomProgressDialog progressDialog = null;
	AsyncTask<File, Integer, ArrayList<String>> torrentTask;

	Handler mHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			// TODO Auto-generated method stub
			switch (msg.what) {
			case Constants.WIFI_CLOSE:
				Toast.makeText(TorrentList1.this, "请检查WIFI是否连接哦～",
						Toast.LENGTH_SHORT).show();
				break;
			case Constants.CANCLE_SCAN_TORRENT_TASK:
				Toast.makeText(TorrentList1.this, "种子播放被取消了，是不想看了么～",
						Toast.LENGTH_SHORT).show();
			case Constants.MESSAGE_ERROR:
				Toast.makeText(TorrentList1.this, "无法获取服务器信息",
						Toast.LENGTH_SHORT).show();
			default:
				break;
			}
		}

	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.torrent_list);
		ListView lvTorrent = (ListView) findViewById(R.id.torrentlist);
		/*
		 * ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
		 * android.R.layout.simple_list_item_1,
		 * FileToStr(LocalTorrentFragment.fileList));
		 */
		TorrentListAdapter adapter = new TorrentListAdapter(this,
				LocalTorrentFragment.fileList);

		lvTorrent.setAdapter(adapter);

		lvTorrent.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO 走解析种子文件流程
				File torrentFile = LocalTorrentFragment.fileList.get(position);
				// 经过验证 fileList.getPostion 和 getPosition.getName是一样的 。
				/*
				 * 开启 AsyncTask 解析 种子文件。
				 */
				torrentTask = new ParseUriTask().execute(torrentFile);
			}
		});
	}

	private class ParseUriTask extends
			AsyncTask<File, Integer, ArrayList<String>> {
		// ProgressDialog pg ;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			startProgressDialog(true);
			progressDialog
					.setOnCancelListener(new DialogInterface.OnCancelListener() {

						@Override
						public void onCancel(DialogInterface dialog) {
							// TODO Auto-generated method stub
							torrentTask.cancel(true);
							Message msg = new Message();
							msg.what = Constants.CANCLE_SCAN_TORRENT_TASK;
							mHandler.sendMessage(msg);
						}
					});
		}

		/**
		 * 判断WIFI是否连接
		 * 
		 * @param inContext
		 * @return
		 */
		public boolean isWiFiActive(Context inContext) {
			WifiManager mWifiManager = (WifiManager) inContext
					.getSystemService(Context.WIFI_SERVICE);
			WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
			int ipAddress = wifiInfo == null ? 0 : wifiInfo.getIpAddress();
			if (mWifiManager.isWifiEnabled() && ipAddress != 0) {
				System.out.println("**** WIFI is on");
				return true;
			} else {
				System.out.println("**** WIFI is off");
				return false;
			}
		}

		@Override
		protected ArrayList<String> doInBackground(File... params) {
			// TODO Auto-generated method stub
			try {
				FileInputStream in = new FileInputStream(params[0]);
				String btihFromTorrent = new BTihCalculator(in).doCalculate();

				if (btihFromTorrent != null) {
					BTihItem btih = new BTihItem();
					btih.BTih = btihFromTorrent;

					Log.v("BTIH :", btihFromTorrent);
					Log.v("BTIH :", btihFromTorrent);

					if (btih.BTih != null
							&& isWiFiActive(TorrentList1.this) == true) {

						List<BTXunleiItem> btItems = mFinder.getList(btih);

						if (btItems.size() > 0) {
							List<VideoItem> vis = mFinder
									.getPlayableUrls(btItems.get(0));
							Log.v("VIS_LIST", vis.toString());
							if (vis.size() != 0) {
								if (vis.get(0).url
										.equals("response_error_message")) {
									Message msg = new Message();
									msg.what = Constants.MESSAGE_ERROR;
									mHandler.sendMessage(msg);
								} else {
									ArrayList<String> uris = new ArrayList<String>();
									for (int i = 0; i < vis.size(); i++) {
										uris.add(vis.get(i).url);
									}

									return uris;
								}
							}else{
								Message msg = new Message();
								msg.what = Constants.MESSAGE_ERROR;
								mHandler.sendMessage(msg);
							}
						}

					} else {
						Message msg = new Message();
						msg.what = Constants.WIFI_CLOSE;
						mHandler.sendMessage(msg);
					}

				}

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(ArrayList<String> result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// progressDialog.dismiss();
			stopProgressDialog();

			if (result != null) {
				Intent i = new Intent(TorrentList1.this,
						VideoViewPlayingActivity.class);
				i.putExtra(VideoViewPlayingActivity.PLAY_URI, result);
				i.putExtra(VideoViewPlayingActivity.SUBTITLE_URI, result);

				Log.v("### PLAY_URL ###", result.get(result.size() - 1));

				startActivity(i);
			} else {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("video_cannot_paly", "视频无法播放");
				MobclickAgent.onEvent(TorrentList1.this, "can_not_play", map);
				Toast.makeText(TorrentList1.this, "该视频无法播放", Toast.LENGTH_SHORT)
						.show();

			}

		}

		private void startProgressDialog(final boolean setCancelable) {
			if (progressDialog == null) {
				progressDialog = CustomProgressDialog
						.createDialog(TorrentList1.this);
				progressDialog.setMessage("正在玩命加载中...");
				progressDialog.setCancelable(setCancelable);
				/*
				 * progressDialog.setOnCancelListener(new OnCancelListener() {
				 * 
				 * @Override public void onCancel(DialogInterface dialog) { //
				 * TODO Auto-generated method stub if(setCancelable == true){
				 * progressDialog.setCancelable(true); }else{
				 * progressDialog.setCancelable(false); } } });
				 */
				// mProgressDialog.setCanceledOnTouchOutside(false);
			}

			progressDialog.show();
		}

		private void stopProgressDialog() {
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
		}

	}

	public String[] FileToStr(List<File> f) {
		// f = LocalTorrentFragment.fileList
		ArrayList<String> listStr = new ArrayList<String>();
		// set ? 不重复？ Google
		Set<String> set = new HashSet<String>();

		for (int i = 0; i < f.size(); i++) {
			String nameString = f.get(i).getName();
			set.add(nameString);
		}
		Log.v("LIST_SIZE", String.valueOf(listStr.size()));
		return set.toArray(new String[0]);
	}

}
