package com.hdx.btlive.zxing.camera.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hdx.btlive.R;
import com.hdx.btlive.constants.Constants;
import com.hdx.btlive.finder.BaseFinder;
import com.hdx.btlive.finder.MyFinder;
import com.hdx.btlive.player.VideoViewPlayingActivity;
import com.hdx.btlive.sniffer.BaseSniffer;
import com.hdx.btlive.sniffer.BaseSniffer.BTXunleiItem;
import com.hdx.btlive.sniffer.BaseSniffer.BTihItem;
import com.hdx.btlive.sniffer.BaseSniffer.VideoItem;
import com.hdx.btlive.ui.selfdialog.CustomProgressDialog;
import com.umeng.analytics.MobclickAgent;

/**
 * 此类为DEMO类，该功能已经在LocalTorrentFragment.java中得到实现
 * @author sadieyu
 *
 */


/*public class MainActivity extends Activity {
	SharedPreferences settingShare;
	BaseFinder mFinder = new MyFinder();
	BaseSniffer mSniffer;
	Bundle bundle;
	ArrayList<String> uris;
	List<BTXunleiItem> vis;
	private final static int SCANNIN_GREQUEST_CODE = 1;
	*//**
	 * 显示扫描结果
	 *//*
	private TextView mTextView;
	*//**
	 * 显示扫描拍的图片
	 *//*
	private ImageView mImageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mTextView = (TextView) findViewById(R.id.result);
		mImageView = (ImageView) findViewById(R.id.qrcode_bitmap);

		// 点击按钮跳转到二维码扫描界面，这里用的是startActivityForResult跳转
		// 扫描完了之后调到该界面
		Button mButton = (Button) findViewById(R.id.button1);
		mButton.setOnClickListener(new ViewOnClick());
		mTextView.setOnClickListener(new ViewOnClick());
	}

	*//**
	 * Handler .
	 *//*
	public Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Constants.BTSCG_OK:
				Constants.BT_RESULT_OK = true;
				// mProgressDialog.dismiss();
				break;
			case Constants.TKCN_OK:
				Constants.TK_RESULT_OK = true;
				// mProgressDialog.dismiss();
				break;
			case Constants.MESSAGE_ERROR:
				Toast.makeText(MainActivity.this, "未找到该资源的播放地址，请尝试其他。",
						Toast.LENGTH_SHORT).show();
				break;
			case Constants.WIFI_CLOSE:
				Toast.makeText(MainActivity.this, "请检查是否开启WIFI哦～",
						Toast.LENGTH_LONG).show();
				break;
			case Constants.IS_MAGNET_OR_NOT:
				Toast.makeText(MainActivity.this, " 亲，你确定扫描结果是磁力链么？～",
						Toast.LENGTH_LONG).show();
				break;
			default:
				break;
			}
		}
	};

	*//**
	 * 判断WIFI是否连接
	 * 
	 * @param inContext
	 * @return
	 *//*
	public boolean isWiFiActive(Context inContext) {
		WifiManager mWifiManager = (WifiManager) inContext
				.getSystemService(Context.WIFI_SERVICE);
		WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
		int ipAddress = wifiInfo == null ? 0 : wifiInfo.getIpAddress();
		if (mWifiManager.isWifiEnabled() && ipAddress != 0) {
			System.out.println("**** WIFI is on");
			return true;
		} else {
			System.out.println("**** WIFI is off");
			return false;
		}
	}

	// View 点击时间监听的实现
	public class ViewOnClick implements OnClickListener {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.button1:
				Intent intent = new Intent();
				intent.setClass(MainActivity.this, MipcaActivityCapture.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivityForResult(intent, SCANNIN_GREQUEST_CODE);
				break;
			case R.id.result:
				String magnet = bundle.getString("result");

				new ParseUriTask().execute(magnet);

				Log.v("MAGNET", magnet);
				//
				// if (magnet.length() >= 64) {
				// btih.BTih = magnet.substring(24, 63);
				//
				// } else {
				// Toast.makeText(MainActivity.this, "一定要正确得到正确的磁力链哦～",
				// Toast.LENGTH_SHORT).show();
				// }
				// Log.v("BTIH", btih.BTih);
				break;

			default:
				break;
			}

		}

	}

	private class ParseUriTask extends
			AsyncTask<String, Integer, ArrayList<String>> {
		// ProgressDialog pg ;
		CustomProgressDialog progressDialog = null;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			startProgressDialog(false);
		}

		*//**
		 * 判断WIFI是否连接
		 * 
		 * @param inContext
		 * @return
		 *//*
		public boolean isWiFiActive(Context inContext) {
			WifiManager mWifiManager = (WifiManager) inContext
					.getSystemService(Context.WIFI_SERVICE);
			WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
			int ipAddress = wifiInfo == null ? 0 : wifiInfo.getIpAddress();
			if (mWifiManager.isWifiEnabled() && ipAddress != 0) {
				System.out.println("**** WIFI is on");
				return true;
			} else {
				System.out.println("**** WIFI is off");
				return false;
			}
		}

		@Override
		protected ArrayList<String> doInBackground(String... params) {
			// TODO Auto-generated method stub
			BTihItem btih = new BTihItem();
			String magnetToSub = params[0];
			if (magnetToSub.length() > 66) {
				btih.BTih = params[0].substring(24, 64);

				Log.v("BTIH", btih.BTih);

				if (isWiFiActive(MainActivity.this) == true) {

					List<BTXunleiItem> btItems = mFinder.getList(btih);

					if (btItems.size() > 0) {
						List<VideoItem> vis = mFinder.getPlayableUrls(btItems
								.get(0));
						Log.v("VIS_LIST", vis.toString());
						Log.v("VIS", vis.get(0).url);
						if (vis.size() != 0) {
							if (vis.get(0).url.equals("response_error_message")) {
								Toast.makeText(MainActivity.this, "无法获取服务器信息",
										Toast.LENGTH_SHORT).show();
							} else {
								ArrayList<String> uris = new ArrayList<String>();
								for (int i = 0; i < vis.size(); i++) {
									uris.add(vis.get(i).url);
								}

								return uris;
							}
						}
					}

				} else {
					Message msg = new Message();
					msg.what = Constants.WIFI_CLOSE;
					mHandler.sendMessage(msg);
				}

			} else {
				Message msg = new Message();
				msg.what = Constants.IS_MAGNET_OR_NOT;
				mHandler.sendMessage(msg);
			}
			return null;

		}

		@Override
		protected void onPostExecute(ArrayList<String> result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// progressDialog.dismiss();
			stopProgressDialog();

			if (result != null) {
				Intent i = new Intent(MainActivity.this,
						VideoViewPlayingActivity.class);
				i.putExtra(VideoViewPlayingActivity.PLAY_URI, result);
				i.putExtra(VideoViewPlayingActivity.SUBTITLE_URI, result);

				Log.v("### PLAY_URL ###", result.get(result.size() - 1));

				startActivity(i);
			} else {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("video_cannot_paly", "视频无法播放");
				MobclickAgent.onEvent(MainActivity.this, "can_not_play", map);
				Toast.makeText(MainActivity.this, "该视频无法播放", Toast.LENGTH_SHORT)
						.show();
			}

		}

		private void startProgressDialog(final boolean setCancelable) {
			if (progressDialog == null) {
				progressDialog = CustomProgressDialog
						.createDialog(MainActivity.this);
				progressDialog.setMessage("正在加载中...");
				progressDialog.setCancelable(setCancelable);
			}

			progressDialog.show();
		}

		private void stopProgressDialog() {
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case SCANNIN_GREQUEST_CODE:
			if (resultCode == RESULT_OK) {
				bundle = data.getExtras();
				// 显示扫描到的内容
				mTextView.setText(bundle.getString("result"));
				// 显示
				mImageView.setImageBitmap((Bitmap) data
						.getParcelableExtra("bitmap"));
			}
			break;
		}
	}

}
*/