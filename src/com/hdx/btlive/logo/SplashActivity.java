package com.hdx.btlive.logo;

import java.util.Timer;
import java.util.TimerTask;

import net.youmi.android.AdManager;
import android.content.Intent;
import android.os.Bundle;
import cn.waps.AppConnect;

import com.hdx.btlive.R;
import com.hdx.btlive.ui.BaseActivity;
import com.hdx.btlive.ui.MainFragmentActivity;
import com.umeng.fb.FeedbackAgent;

public class SplashActivity extends BaseActivity{
	FeedbackAgent agent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        /*		
        MobclickAgent.setDebugMode(true);
		UmengUpdateAgent.update(this);
		*/
		AppConnect.getInstance("c672a395e2b7941db99acb84aed87b20", "WAPS", this);
		 
		 // 初始化应用的发布ID和密钥，以及设置测试模式  YouMi
        AdManager.getInstance(this).init("74f95de6a20a82c7","d6c5c14ac3c56577", false); 
		
		setContentView(R.layout.activity_splash);
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
//				Intent i = new Intent(SplashActivity.this, Main.class);
				Intent i = new Intent(SplashActivity.this, MainFragmentActivity.class);
				startActivity(i);
				finish();
			}
		}, 2000);

	}
}
