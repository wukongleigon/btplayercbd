package com.hdx.btlive.ui;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hdx.btlive.R;
import com.hdx.btlive.constants.Constants;
import com.hdx.btlive.finder.BaseFinder;
import com.hdx.btlive.finder.MyFinder;
import com.hdx.btlive.localtorrent.scanner.TorrentList1;
import com.hdx.btlive.player.VideoViewPlayingActivity;
import com.hdx.btlive.sniffer.BaseSniffer;
import com.hdx.btlive.sniffer.BaseSniffer.BTXunleiItem;
import com.hdx.btlive.sniffer.BaseSniffer.BTihItem;
import com.hdx.btlive.sniffer.BaseSniffer.VideoItem;
import com.hdx.btlive.ui.selfdialog.CustomProgressDialog;
import com.hdx.btlive.zxing.camera.main.MipcaActivityCapture;
import com.umeng.analytics.MobclickAgent;

public class LocalTorrentFragment extends Fragment {
	public static List<File> fileList;
	View root;
	SharedPreferences settingShare;
	BaseFinder mFinder = new MyFinder();
	BaseSniffer mSniffer;
	Bundle bundle;
	ArrayList<String> uris;
	List<BTXunleiItem> vis;
	private final static int SCANNIN_GREQUEST_CODE = 3;
	/**
	 * 显示扫描结果
	 */
	private TextView mTextViewResult, mTVResultDesc;
	CustomProgressDialog progressDialog = null;
	AsyncTask<Integer, Integer, String[]> scannerLocalTorrentFiles;
	AsyncTask<String, Integer, ArrayList<String>> parseUriTask;

	Handler mHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case Constants.CANCLE_SCAN_TORRENT_TASK:

				Toast.makeText(getActivity(), "扫描被取消了是不小心的么 ～",
						Toast.LENGTH_SHORT).show();
				break;
			case Constants.MESSAGE_ERROR:
				Toast.makeText(getActivity(), "未找到该资源的播放地址，请尝试其他。",
						Toast.LENGTH_SHORT).show();
				break;
			case Constants.WIFI_CLOSE:
				Toast.makeText(getActivity(), "请检查是否开启WIFI哦～",
						Toast.LENGTH_LONG).show();
				break;
			case Constants.IS_MAGNET_OR_NOT:
				Toast.makeText(getActivity(), " 亲，你确定扫描结果是磁力链么？～",
						Toast.LENGTH_LONG).show();
				break;
			case Constants.CANCLE_PARSE_URI_TASK:
				Toast.makeText(getActivity(), "天啊，播放被取消啦？～",
						Toast.LENGTH_LONG).show();
				break;
			default:
				break;
			}
		}
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		root = inflater.inflate(R.layout.main_scanner, container, false);

		Button btnScanner = (Button) root.findViewById(R.id.button_scanner);
		mTextViewResult = (TextView) root.findViewById(R.id.result);
		mTVResultDesc = (TextView) root.findViewById(R.id.result_describe);

		ImageView qrScanner = (ImageView) root
				.findViewById(R.id.iv_img_torrent);
		fileList = new ArrayList<File>();

		mTextViewResult.setOnClickListener(new ViewOnClick());
		qrScanner.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*
				 * Intent intent = new Intent(getActivity(),
				 * MainActivity.class); getActivity().startActivity(intent);
				 */
				Intent intent = new Intent();
				intent.setClass(getActivity(), MipcaActivityCapture.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivityForResult(intent, SCANNIN_GREQUEST_CODE);

			}
		});
		btnScanner.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Toast.makeText(getActivity(), "BUTTON BUTTON",
				// Toast.LENGTH_SHORT).show();
				// 异步搜做SDCard中的所有种子文件。
				scannerLocalTorrentFiles = new AsyncTask<Integer, Integer, String[]>() {

					protected void onPreExecute() {
						super.onPreExecute();
						startProgressDialog(true);
						progressDialog
								.setOnCancelListener(new OnCancelListener() {

									@Override
									public void onCancel(DialogInterface dialog) {
										// TODO Auto-generated method stub
										scannerLocalTorrentFiles.cancel(true);
										Message msg = new Message();
										msg.what = Constants.CANCLE_SCAN_TORRENT_TASK;
										mHandler.sendMessage(msg);
									}
								});
					}

					protected String[] doInBackground(Integer... params) {
						if (!android.os.Environment.getExternalStorageState()
								.equals(android.os.Environment.MEDIA_MOUNTED)) {

						} else {
							GetFiles(Environment.getExternalStorageDirectory());
						}
						return null;
					}

					protected void onPostExecute(String[] result) {
						// dialog.dismiss();
						stopProgressDialog();

						if (fileList.size() == 0) {
							Toast.makeText(getActivity(), "很遗憾没有找到种子文件.",
									Toast.LENGTH_SHORT).show();
						} else {
							// 打印SDCard中素有 .torrent 文件
							for (int i = 0; i < fileList.size(); i++) {
								Log.v("FILE_NAME_IN_SDCARD", fileList.get(i)
										.getPath());
							}

							Toast.makeText(getActivity(), "扫描完成啦～",
									Toast.LENGTH_SHORT).show();
							Intent in = new Intent(getActivity(),
									TorrentList1.class);
							startActivity(in);
						}
						super.onPostExecute(result);
					}

					private void startProgressDialog(final boolean setCancelable) {
						if (progressDialog == null) {
							progressDialog = CustomProgressDialog
									.createDialog(getActivity());
							progressDialog.setMessage("正在挣命加载中...");
							progressDialog.setCancelable(setCancelable);

							/*
							 * progressDialog.setOnCancelListener(new
							 * OnCancelListener() {
							 * 
							 * @Override public void onCancel(DialogInterface
							 * dialog) { // TODO Auto-generated method stub
							 * if(setCancelable == true){
							 * progressDialog.setCancelable(true); }else{
							 * progressDialog.setCancelable(false); } } });
							 */
							// mProgressDialog.setCanceledOnTouchOutside(false);
						}

						progressDialog.show();
					}

					private void stopProgressDialog() {
						if (progressDialog != null) {
							progressDialog.dismiss();
							progressDialog = null;
						}
					}
				}.execute(0);
			}
		});

		return root;

	}

	/**
	 * 判断WIFI是否连接
	 * 
	 * @param inContext
	 * @return
	 */
	public boolean isWiFiActive(Context inContext) {
		WifiManager mWifiManager = (WifiManager) inContext
				.getSystemService(Context.WIFI_SERVICE);
		WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
		int ipAddress = wifiInfo == null ? 0 : wifiInfo.getIpAddress();
		if (mWifiManager.isWifiEnabled() && ipAddress != 0) {
			System.out.println("**** WIFI is on");
			return true;
		} else {
			System.out.println("**** WIFI is off");
			return false;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		// super.onActivityResult(requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case SCANNIN_GREQUEST_CODE:
			if (resultCode == getActivity().RESULT_OK) {
				mTextViewResult.setVisibility(View.VISIBLE);
				mTVResultDesc.setVisibility(View.VISIBLE);
				bundle = data.getExtras();
				// 显示扫描到的内容
				mTextViewResult.setBackgroundResource(android.R.color.black);
				mTextViewResult.setText(bundle.getString("result"));
				// 显示
				mTVResultDesc.setText("点击下面磁力链开始播放 ：");
			}
			break;
		}
	}

	// View 点击时间监听的实现
	public class ViewOnClick implements OnClickListener {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.result:
				String magnet = bundle.getString("result");
				mTextViewResult.setText("");
				mTextViewResult.setVisibility(View.GONE);
				mTVResultDesc.setVisibility(View.GONE);
		        parseUriTask = new ParseUriTask().execute(magnet);

				Log.v("MAGNET", magnet);

				break;

			default:
				break;
			}

		}

	}

	private class ParseUriTask extends
			AsyncTask<String, Integer, ArrayList<String>> {
		// ProgressDialog pg ;
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			startProgressDialog(true);
			progressDialog.setOnCancelListener(new OnCancelListener() {
				
				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
				   parseUriTask.cancel(true);
				   Message msg = new Message();
				   msg.what = Constants.CANCLE_PARSE_URI_TASK;
				   mHandler.sendMessage(msg);
				}
			});

		}

		@Override
		protected ArrayList<String> doInBackground(String... params) {
			// TODO Auto-generated method stub
			BTihItem btih = new BTihItem();
			String magnetToSub = params[0];
			if (magnetToSub.length() > 66) {
				btih.BTih = params[0].substring(24, 64);

				Log.v("BTIH", btih.BTih);

				if (isWiFiActive(getActivity()) == true) {

					List<BTXunleiItem> btItems = mFinder.getList(btih);

					if (btItems.size() > 0) {
						List<VideoItem> vis = mFinder.getPlayableUrls(btItems
								.get(0));
						Log.v("VIS_LIST", vis.toString());
						Log.v("VIS", vis.get(0).url);
						if (vis.size() != 0) {
							if (vis.get(0).url.equals("response_error_message")) {
								Toast.makeText(getActivity(), "无法获取服务器信息",
										Toast.LENGTH_SHORT).show();
							} else {
								ArrayList<String> uris = new ArrayList<String>();
								for (int i = 0; i < vis.size(); i++) {
									uris.add(vis.get(i).url);
								}

								return uris;
							}
						}
					}

				} else {
					Message msg = new Message();
					msg.what = Constants.WIFI_CLOSE;
					mHandler.sendMessage(msg);
				}

			} else {
				Message msg = new Message();
				msg.what = Constants.IS_MAGNET_OR_NOT;
				mHandler.sendMessage(msg);
			}
			return null;

		}

		@Override
		protected void onPostExecute(ArrayList<String> result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			// progressDialog.dismiss();
			stopProgressDialog();

			if (result != null) {
				Intent i = new Intent(getActivity(),
						VideoViewPlayingActivity.class);
				i.putExtra(VideoViewPlayingActivity.PLAY_URI, result);
				i.putExtra(VideoViewPlayingActivity.SUBTITLE_URI, result);

				Log.v("### PLAY_URL ###", result.get(result.size() - 1));

				startActivity(i);
			} else {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("video_cannot_paly", "视频无法播放");
				MobclickAgent.onEvent(getActivity(), "can_not_play", map);
				Toast.makeText(getActivity(), "该视频无法播放", Toast.LENGTH_SHORT)
						.show();
			}

		}

		private void startProgressDialog(final boolean setCancelable) {
			if (progressDialog == null) {
				progressDialog = CustomProgressDialog
						.createDialog(getActivity());
				progressDialog.setMessage("正在加载中...");
				progressDialog.setCancelable(setCancelable);
			}

			progressDialog.show();
		}

		private void stopProgressDialog() {
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
		}

	}

	/**
	 * 获取文件列表
	 * 
	 * @param filePath
	 */
	public void GetFiles(File filePath) {

		File[] files = filePath.listFiles();

		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					GetFiles(files[i]);
				} else {
					if (files[i].getName().toLowerCase().endsWith(".torrent")) {
						fileList.add(files[i]);
					}
				}
			}
		}
	}
}
