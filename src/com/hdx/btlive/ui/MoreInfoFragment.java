package com.hdx.btlive.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import cn.waps.AppConnect;

import com.hdx.btlive.R;
import com.umeng.fb.FeedbackAgent;

public class MoreInfoFragment extends Fragment {
	View root;
	FeedbackAgent agent;
	public static final int REQ_SYS_SETTINGS = 0x001;
	// private String cbpKey = null;
	private String btscgKey = null;
	private String torrentKey = null;
	private String torrentcnKey = null;
	private String oabtKey = null;
	private String btdiggKey = null;
	private String lpKey = null;

	private static final int REQ_SYSTEM_SETTINGS = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		root = inflater.inflate(R.layout.moreinfo_fragment, container, false);

		Button btn = (Button) root.findViewById(R.id.btn_give_suggest);
		Button btnSetting = (Button) root.findViewById(R.id.btn_setting);
		Button btnMoreApps = (Button) root.findViewById(R.id.more_apps);

		agent = new FeedbackAgent(getActivity());
		agent.sync();
		btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				agent.startFeedbackActivity();
			}
		});
		btnSetting.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// 转到Settings设置界面
				startActivityForResult(new Intent(getActivity(),
						MySettings.class), REQ_SYSTEM_SETTINGS);
			}
		});
		btnMoreApps.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// 显示推荐列表（游戏）
				AppConnect.getInstance(getActivity()).showGameOffers(getActivity());
			}
		});

		return root;

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (resultCode == REQ_SYSTEM_SETTINGS) {

			torrentKey = getResources().getString(R.string.c_torrentkitty);
			btscgKey = getResources().getString(R.string.c_btscg);
			torrentcnKey = getResources().getString(R.string.c_torrentkittycn);
			oabtKey = getResources().getString(R.string.c_oabt);
			btdiggKey = getResources().getString(R.string.c_btdigg);
			lpKey = getResources().getString(R.string.lp_key);

			// 取得应用的 SharedPreference
			SharedPreferences sp = PreferenceManager
					.getDefaultSharedPreferences(getActivity());
			Boolean btSourceV = sp.getBoolean(btscgKey, true);
			Boolean tkSourceV = sp.getBoolean(torrentKey, false);
			Boolean tkcnSourceV = sp.getBoolean(torrentcnKey, false);
			Boolean oabtSourceV = sp.getBoolean(oabtKey, false);
			Boolean btdiggSourceV = sp.getBoolean(btdiggKey, false);
			// String setLine = sp.getString("lpKey");
			String setLine = sp.getString(lpKey, "HtpFuns");

//			Log.v("### MOREINFO ###", "BTSCG :" + btSourceV + "\n" + "TK :"
//					+ tkSourceV + "\n" + "TKCN :" + tkcnSourceV + "\n"
//					+ "LINE_NUM :" + setLine);

			/*
			 * Constants.WHICH_LINE = setLine; Constants.isSetBtscg = btSourceV;
			 * Constants.isSetTk = tkSourceV;
			 */
			// 1. 创建sharedPreference
			// SharedPreferences settingShare =
			// getSharedPreferences("setting",0);
			SharedPreferences settingShare = getActivity()
					.getSharedPreferences("setting", 0);
			// 2、让setting处于编辑状态
			SharedPreferences.Editor editor = settingShare.edit();
			// 3、存放数据
			editor.putString("WHICH_LINE", setLine);
			editor.putBoolean("IS_SET_BTSCG", btSourceV);
			editor.putBoolean("IS_SET_TK", tkSourceV);
			editor.putBoolean("IS_SET_TKCN", tkcnSourceV);
			editor.putBoolean("IS_SET_OABT", oabtSourceV);
			editor.putBoolean("IS_SET_BTDIGG", btdiggSourceV);
			// 4、完成提交
			editor.commit();

		} else {
			// 其他Intent返回的结果
		}
	}
}
