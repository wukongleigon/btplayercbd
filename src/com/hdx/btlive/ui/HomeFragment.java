package com.hdx.btlive.ui;

import java.util.HashMap;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import cn.waps.AdView;

import com.hdx.btlive.R;
import com.hdx.btlive.search.SearchResultActivity;
import com.umeng.analytics.MobclickAgent;

public class HomeFragment extends Fragment {
	String keyword;
	ProgressDialog pd; // 进度条对话框
	EditText mSearchEdit;
	Button btnSearch, openBrowser;
	ImageView scanner;
	LinearLayout ll, llHot1, llHot2, llHot3, llHot4;
	InputMethodManager inputManager;
	Button hot1, hot2, hot3, hot4, hot5, hot6, hot7, hot8, hot9, hot10, hot11,
			hot12;
	LinearLayout layout;
	ImageView imgTK, imgBTSCG;
	TextView tv01;
	LinearLayout container;
	View root;
	public static final int GET_INFO_FROM_SCJY = 0x00;// 请求圣城家园列表常量
	protected Animation left_in_animation, right_in_animation;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (container == null) {

		}
		root = inflater.inflate(R.layout.home_linearlayout, container, false);

		// 广告
		LinearLayout containerAds = (LinearLayout) root
				.findViewById(R.id.AdLinearLayout);
		new AdView(getActivity(), containerAds).DisplayAd();

		initUI();

		/**
		 * 设置按钮动画
		 */
		left_in_animation = AnimationUtils.loadAnimation(getActivity(),
				R.anim.hot_left_in);
		right_in_animation = AnimationUtils.loadAnimation(getActivity(),
				R.anim.hot_right_in);
		llHot1.startAnimation(left_in_animation);
		llHot3.startAnimation(left_in_animation);
		llHot2.startAnimation(right_in_animation);
		llHot4.startAnimation(right_in_animation);

		//

		btnSearch.setOnClickListener(new OnViewClick());
		hot1.setOnClickListener(new OnViewClick());
		hot2.setOnClickListener(new OnViewClick());
		hot3.setOnClickListener(new OnViewClick());
		hot4.setOnClickListener(new OnViewClick());
		hot5.setOnClickListener(new OnViewClick());
		hot6.setOnClickListener(new OnViewClick());
		hot7.setOnClickListener(new OnViewClick());
		hot8.setOnClickListener(new OnViewClick());
		hot9.setOnClickListener(new OnViewClick());
		hot10.setOnClickListener(new OnViewClick());
		hot11.setOnClickListener(new OnViewClick());
		hot12.setOnClickListener(new OnViewClick());
		openBrowser.setOnClickListener(new OnViewClick());
		// 点击搜索图标 开启二维码功能
		// scanner.setOnClickListener(new OnViewClick());
		mSearchEdit.setOnEditorActionListener(new GetOnEditorActivon());

		return root;
	}

	// 初始化 UI
	protected void initUI() {
		// TODO Auto-generated method stub
		btnSearch = (Button) root.findViewById(R.id.btn_serarch);
		mSearchEdit = (EditText) root.findViewById(R.id.edt_search_info);
		scanner = (ImageView) root.findViewById(R.id.scanner);
		hot1 = (Button) root.findViewById(R.id.btn_hot1);
		hot2 = (Button) root.findViewById(R.id.btn_hot2);
		hot3 = (Button) root.findViewById(R.id.btn_hot3);
		hot4 = (Button) root.findViewById(R.id.btn_hot4);
		hot5 = (Button) root.findViewById(R.id.btn_hot5);
		hot6 = (Button) root.findViewById(R.id.btn_hot6);
		hot7 = (Button) root.findViewById(R.id.btn_hot7);
		hot8 = (Button) root.findViewById(R.id.btn_hot8);
		hot9 = (Button) root.findViewById(R.id.btn_hot9);
		hot10 = (Button) root.findViewById(R.id.btn_hot10);
		hot11 = (Button) root.findViewById(R.id.btn_hot11);
		hot12 = (Button) root.findViewById(R.id.btn_hot12);

		// 自定义View HomeLayout <--> home.xml
		// layout = (HomeLayout) findViewById(R.id.root_layout);
		imgTK = (ImageView) root.findViewById(R.id.imageView1);
		imgBTSCG = (ImageView) root.findViewById(R.id.imageView2);

		ll = (LinearLayout) root.findViewById(R.id.linearLayout2);

		llHot1 = (LinearLayout) root.findViewById(R.id.ll_hot_1);
		llHot2 = (LinearLayout) root.findViewById(R.id.ll_hot_2);
		llHot3 = (LinearLayout) root.findViewById(R.id.ll_hot_3);
		llHot4 = (LinearLayout) root.findViewById(R.id.ll_hot_4);

		openBrowser = (Button) root.findViewById(R.id.open_browser);
		tv01 = (TextView) root.findViewById(R.id.TextView01);

	}

	public class OnViewClick implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			// Umeng 统计
			MobclickAgent.onEvent(getActivity(), "SerachButton");
			 
			switch (v.getId()) {
			case R.id.btn_serarch:
				// 搜索按钮
				keyword = mSearchEdit.getText().toString();
				if(!keyword.trim().equals("")){
					// Umeng 统计关键词搜索
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("searchKeyWord", keyword);
					MobclickAgent.onEvent(getActivity(), "skeyword", map);
					
					Intent i = new Intent(getActivity(),SearchResultActivity.class);
					i.putExtra(SearchResultActivity.SEARCH_KEYWORD, keyword);
					startActivity(i);
					mSearchEdit.setText("");
				}else{
					Toast.makeText(getActivity(), "请输入搜索内容。", Toast.LENGTH_SHORT).show();
				}
					break;
			case R.id.btn_hot1:
				searchByHotWords(hot1);
				break;
			case R.id.btn_hot2:
				searchByHotWords(hot2);
				break;
			case R.id.btn_hot3:
				searchByHotWords(hot3);
				break;
			case R.id.btn_hot4:
				searchByHotWords(hot4);
				break;
			case R.id.btn_hot5:
				searchByHotWords(hot5);
				break;
			case R.id.btn_hot6:
				searchByHotWords(hot6);
				break;
			case R.id.btn_hot7:
				searchByHotWords(hot7);
				break;
			case R.id.btn_hot8:
				searchByHotWords(hot8);
				break;
			case R.id.btn_hot9:
				searchByHotWords(hot9);
				break;
			case R.id.btn_hot10:
				searchByHotWords(hot10);
				break;
			case R.id.btn_hot11:
				searchByHotWords(hot11);
				break;
			case R.id.btn_hot12:
				searchByHotWords(hot12);
				break;
			case R.id.open_browser:
				Uri uri = Uri
						.parse("http://tieba.baidu.com/p/2264615271");
				Intent intent = new Intent(Intent.ACTION_VIEW, uri);
				startActivity(intent);
				break;
			default:
				break;
			}
		}
	}

	// 热词搜索
	public void searchByHotWords(Button button) {
		keyword = button.getText().toString().trim();
		if (keyword.equals("")) {
			Toast.makeText(getActivity(), "请输入要查询的资源名称。", Toast.LENGTH_SHORT)
					.show();
		} else {
			// Umeng 统计关键词搜索
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("searchKeyWord", keyword);
			MobclickAgent.onEvent(getActivity(), "skeyword", map);

			Intent i = new Intent(getActivity(), SearchResultActivity.class);
			i.putExtra(SearchResultActivity.SEARCH_KEYWORD, keyword);
			// SEARCH_ENGIE_ID , 0 代表圣城家园， 1 代表 TorrentKitty
			i.putExtra(SearchResultActivity.SEARCH_ENGINE_ID, 0);
			startActivity(i);
		}
	}

	public class GetOnEditorActivon implements OnEditorActionListener {

		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			// TODO Auto-generated method stub
			switch (actionId) {
			case EditorInfo.IME_ACTION_SEARCH:
				keyword = mSearchEdit.getText().toString().trim();
				if (keyword.equals("")) {
					Toast.makeText(getActivity(), "请输入要查询的资源名称。",
							Toast.LENGTH_SHORT).show();
				} else {
					// Umeng 统计关键词搜索
					HashMap<String, String> map = new HashMap<String, String>();
					map.put("searchKeyWord", keyword);
					MobclickAgent.onEvent(getActivity(), "skeyword", map);
					((InputMethodManager) mSearchEdit.getContext()
							.getSystemService(Context.INPUT_METHOD_SERVICE))
							.hideSoftInputFromWindow(getActivity()
									.getCurrentFocus().getWindowToken(),
									InputMethodManager.HIDE_NOT_ALWAYS);
					Intent i = new Intent(getActivity(),
							SearchResultActivity.class);
					i.putExtra(SearchResultActivity.SEARCH_KEYWORD, keyword);
					// SEARCH_ENGIE_ID , 0 代表圣城家园， 1 代表 TorrentKitty
					i.putExtra(SearchResultActivity.SEARCH_ENGINE_ID, 0);
					startActivity(i);
					mSearchEdit.setText("");
				}
				break;
			default:
				break;
			}
			return false;
		}

	}
}
