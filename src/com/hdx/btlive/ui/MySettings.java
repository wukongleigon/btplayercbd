package com.hdx.btlive.ui;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;

import com.hdx.btlive.R;
import com.umeng.analytics.MobclickAgent;

@SuppressLint("NewApi")
public class MySettings extends PreferenceActivity implements
		OnPreferenceChangeListener, OnPreferenceClickListener {

	SharedPreferences sp;
	SharedPreferences.Editor editor;
	private static final String TAG = "TAG";

	private CheckBoxPreference setBtscg, setTk, setTkCN, setOabt ,setBtdigg;
	private ListPreference lp = null;

	// private String cbpKey = null;
	private String btscgKey = null;
	private String torrentKey = null;
	private String lpKey = null;
	private String torrentcnKey = null;
	private String oabtKey = null;
	private String btdiggKey = null;

	// private String setMediaS
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		/*
		 * sp = getSharedPreferences("SET_SOURCES", MODE_WORLD_READABLE); editor
		 * = sp.edit();
		 */

		// get Keys
		torrentKey = getResources().getString(R.string.c_torrentkitty);
		btscgKey = getResources().getString(R.string.c_btscg);
		torrentcnKey = getResources().getString(R.string.c_torrentkittycn);
		oabtKey = getResources().getString(R.string.c_oabt);
		btdiggKey = getResources().getString(R.string.c_btdigg);
		lpKey = getResources().getString(R.string.lp_key);
		// cbp = (CheckBoxPreference) this.findPreference(cbpKey);
		setBtscg = (CheckBoxPreference) this.findPreference(btscgKey);
		setTk = (CheckBoxPreference) this.findPreference(torrentKey);
		setTkCN = (CheckBoxPreference) this.findPreference(torrentcnKey);
		setOabt = (CheckBoxPreference) this.findPreference(oabtKey);
		setBtdigg = (CheckBoxPreference)this.findPreference(btdiggKey);
		
		lp = (ListPreference) this.findPreference(lpKey);
		// 设置监听
		setUpListener();
	}

	/**
	 * 设置监听
	 */
	private void setUpListener() {
		// 点击监听
		setBtscg.setOnPreferenceClickListener(this);
		setTk.setOnPreferenceClickListener(this);
		setTkCN.setOnPreferenceClickListener(this);
		setOabt.setOnPreferenceClickListener(this);
		setBtdigg.setOnPreferenceClickListener(this);
		lp.setOnPreferenceClickListener(this);
		// 状态改变监听
		setBtscg.setOnPreferenceChangeListener(this);
		setTk.setOnPreferenceChangeListener(this);
		setTkCN.setOnPreferenceChangeListener(this);
		setOabt.setOnPreferenceChangeListener(this);
		setBtdigg.setOnPreferenceChangeListener(this);
		lp.setOnPreferenceChangeListener(this);
	}

	/**
	 * 当监听的选项状态发生了改变的时候
	 */
	@Override
	public boolean onPreferenceChange(Preference preference, Object newValue) {

		if (preference.getKey().equals(btscgKey)) {

//			Log.v("### BTSCG_MySetting ###", newValue.toString());

		} else if (preference.getKey().equals(torrentKey)) {

//			Log.v("### TK_MySetting ###", newValue.toString());

		} else if (preference.getKey().equals(torrentcnKey)) {
			
//			Log.v("### TKCN_MySetting ###", newValue.toString());
			
		} else if (preference.getKey().equals(oabtKey)) {
			
//			Log.v("### OABT_MySetting ###", newValue.toString());

		} else if (preference.getKey().equals(lpKey)) {
			
//			Log.v("### LINE_MySetting ###", newValue.toString());

		} else if(preference.getKey().equals(btdiggKey)){
			
//			Log.v("### BTDIGG_MySetting ###", newValue.toString());
			
		} else {
			return false;
		}
		return true;
	}

	/**
	 * 当监听的选项被点击的时候
	 */
	@Override
	public boolean onPreferenceClick(Preference preference) {

		if (preference.getKey().equals(btscgKey)) {

		} else if (preference.getKey().equals(torrentKey)) {

		} else if (preference.getKey().equals(torrentcnKey)) {

		} else if (preference.getKey().equals(oabtKey)) {

		} else if (preference.getKey().equals(lpKey)) {

		} else if(preference.getKey().equals(btdiggKey)){
			
		}else {
			// 如果返回false表示不允许被改变
			return false;
		}
		// 返回true表示允许改变
		return true;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		MobclickAgent.onResume(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
