package com.hdx.btlive.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import cn.waps.AdView;
import cn.waps.AppConnect;

import com.hdx.btlive.R;
import com.umeng.analytics.MobclickAgent;

public class MoreGameFragment extends Fragment {

	View root;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		if (container == null) {

		}
		root = inflater.inflate(R.layout.home_linearlayout, container, false);

		// 广告
		LinearLayout containerAds = (LinearLayout) root
				.findViewById(R.id.AdLinearLayout);
		new AdView(getActivity(), containerAds).DisplayAd();

		// 显示推荐列表（游戏）
		AppConnect.getInstance(getActivity()).showGameOffers(getActivity());

		initUI();

		return root;
	}

	// 初始化 UI
	protected void initUI() {
		// TODO Auto-generated method stub

	}
	public class OnViewClick implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			// Umeng 统计
			MobclickAgent.onEvent(getActivity(), "SerachButton");

			switch (v.getId()) {

			default:
				break;
			}
		}
	}
}
