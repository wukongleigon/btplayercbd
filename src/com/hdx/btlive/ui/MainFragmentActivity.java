package com.hdx.btlive.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.inputmethod.InputMethodManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.Toast;
import cn.waps.AppConnect;

import com.hdx.btlive.R;
import com.hdx.btlive.ads.QuitPopAd;

public class MainFragmentActivity extends FragmentActivity {

	// 定义FragmentTabHost对象
	private FragmentTabHost mTabHost;
	private RadioGroup mTabRg;

	private final Class[] fragments = { HomeFragment.class,
			MoreInfoFragment.class,LocalTorrentFragment.class};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_tab);
		// 初始化插屏广告数据
		AppConnect.getInstance(this).initPopAd(this);
		initView();
	}

	private void initView() {
		mTabHost = (FragmentTabHost) findViewById(android.R.id.tabhost);
		mTabHost.setup(this, getSupportFragmentManager(), R.id.realtabcontent);
		// 得到fragment的个数
		int count = fragments.length;
		for (int i = 0; i < count; i++) {
			// 为每一个Tab按钮设置图标、文字和内容
			TabSpec tabSpec = mTabHost.newTabSpec(i + "").setIndicator(i + "");
			// 将Tab按钮添加进Tab选项卡中
			mTabHost.addTab(tabSpec, fragments[i], null);
		}

		mTabRg = (RadioGroup) findViewById(R.id.tab_rg_menu);
		mTabRg.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				
				switch (checkedId) {
				case R.id.tab_rb_1:
					mTabHost.setCurrentTab(0);
					break;
				case R.id.tab_rb_2:
					RadioButton rb2 = (RadioButton)findViewById(R.id.tab_rb_2);
					InputMethodManager  imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//                    imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
                    imm.hideSoftInputFromWindow(rb2.getWindowToken(), 0);
					mTabHost.setCurrentTab(1);
					
					break;
				case R.id.tab_rb_3:
					RadioButton rb3 = (RadioButton)findViewById(R.id.tab_rb_3);
					InputMethodManager imm1 = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//                    imm1.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    imm1.hideSoftInputFromWindow(rb3.getWindowToken(), 0);
					mTabHost.setCurrentTab(2);
					
					break;
				default:
					break;
				}
			}
		});

		mTabHost.setCurrentTab(0);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// 调用退屏广告
		QuitPopAd.getInstance().show(this);
//		exit();
	}
	private void exit() {
		// TODO Auto-generated method stub
		new AlertDialog.Builder(this)
		.setIcon(R.drawable.dialogtitle_icon)
		.setMessage(getResources().getString(R.string.isexit))
		.setTitle(getResources().getString(R.string.dialogtitle))
		.setPositiveButton("是", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
				android.os.Process.killProcess(android.os.Process
						.myPid());
				System.exit(0);
			}
		})
		.setNegativeButton("否", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
			}
		}).show();
	}

	/**
	 * 判断WIFI是否连接
	 * 
	 * @param inContext
	 * @return
	 */
	public  boolean isWiFiActive(Context inContext) {
		WifiManager mWifiManager = (WifiManager) inContext
				.getSystemService(Context.WIFI_SERVICE);
		WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
		int ipAddress = wifiInfo == null ? 0 : wifiInfo.getIpAddress();
		if (mWifiManager.isWifiEnabled() && ipAddress != 0) {
			System.out.println("**** WIFI is on");
			return true;
		} else {
			System.out.println("**** WIFI is off");
			return false;
		}
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
      boolean isNetOpen = isWiFiActive(getApplicationContext());		
      if(isNetOpen == false){
    	  Toast.makeText(getApplicationContext(), "请检查WIFI是否连接哦～", Toast.LENGTH_SHORT).show();
      }
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		AppConnect.getInstance(this).close(); 
		super.onDestroy();
	}

}
