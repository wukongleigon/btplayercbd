package com.hdx.btlive.finder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import android.app.Activity;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.widget.Toast;

import com.hdx.btlive.constants.Constants;
import com.hdx.btlive.sniffer.BaseSniffer;
import com.hdx.btlive.sniffer.BaseSniffer.BTXunleiItem;
import com.hdx.btlive.sniffer.BaseSniffer.BTihItem;
import com.hdx.btlive.sniffer.BaseSniffer.VideoItem;
import com.hdx.btlive.utils.SimpleHttpClient;

public class MyFinder extends BaseFinder {
	JSONObject resp;
	protected String uid = "244647876";
	Activity mContext;
	List<BTXunleiItem> list;

	public MyFinder(Activity mContext) {
		super();
		this.mContext = mContext;
	}

	public MyFinder() {
		super();
	}

	

	@Override
	public List<BTXunleiItem> getList(BTihItem p) {
		String url = String
				.format("http://i.vod.xunlei.com/req_subBT/info_hash/%s/req_num/2000/req_offset/0/",
						p.BTih.toUpperCase());

			resp = SimpleHttpClient.getJSON(url);

		if (resp != null) {
			list = new ArrayList<BaseSniffer.BTXunleiItem>();
			try {
				JSONArray subfile_list = resp.getJSONObject("resp")
						.getJSONArray("subfile_list");
				if (subfile_list != null) {
					for (int i = 0; i < subfile_list.length(); i++) {
						JSONObject o = subfile_list.getJSONObject(i);
						BTXunleiItem vi = new BTXunleiItem(o.getString("name"),
								o.getString("cid"), o.getString("gcid"),
								o.getString("index"));
						list.add(vi);
					}
				}
			} catch (JSONException e) {

			}
		} else {
			// mContext.finish();
			Toast.makeText(mContext, "哎呀我去，网络请求未应答，请重试。", Toast.LENGTH_SHORT)
					.show();
		}

		return list;
	}

	@Override
	public List<VideoItem> getPlayableUrls(BTXunleiItem vi) {
		List<VideoItem> resultList = new ArrayList<BaseSniffer.VideoItem>();
		String url = String.format("http://157.7.138.239/%s/%s", vi.gcid,
				vi.cid);
		String respMf = SimpleHttpClient.get(url);
		if (respMf != null) {
			// wait
			Document doc = Jsoup.parse(respMf);
			Elements command = doc.select("command");
			String[] filetypes = { "play_url", "ts_url", "mp4_url" };
			// 默认从高清开始播放
			// String[] resolutions = { "high_url", "mid_url", "normal_url" };
			// 默认从 普通开始播放
			String[] resolutions = { "normal_url", "mid_url", "high_url" };
			String durl;
			if (command.size() > 0) {
				if (command.get(0).attr("message").equals("ok")) {
					for (int i = 0; i < filetypes.length; i++) {
						Elements items = doc.select(filetypes[i]);
						for (int j = 0; j < resolutions.length; j++) {
							durl = items.get(0).attr(resolutions[0]);
							if (!durl.equals("")) {
								// 这里需要将normal_url进行处理 用null替换掉所有的&amp;
								String dual_play = durl
										.replaceAll("&amp;", "&");
								VideoItem v = new VideoItem();
								v.url = dual_play;
								v.weight = (j + 1) << 8 + (i + 1);
								resultList.add(v);
							}
						}
					}
				} else if (command.get(0).attr("message").equals("error")) {
					VideoItem v = new VideoItem();
					v.errorMsg = Constants.RESPONSE_MESSAGE_ERROR;
					resultList.add(v);
				}
			}
		} else {
			VideoItem v = new VideoItem();
			v.url = Constants.RESPONSE_MESSAGE_ERROR;
			resultList.add(v);
		}

		Collections.sort(resultList, new Comparator<VideoItem>() {

			@Override
			public int compare(VideoItem lhs, VideoItem rhs) {
				return lhs.weight - rhs.weight;
			}
		});
		return resultList;

	}
}