package com.hdx.btlive.finder;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;

import com.hdx.btlive.sniffer.BaseSniffer;
import com.hdx.btlive.sniffer.BaseSniffer.BTXunleiItem;
import com.hdx.btlive.sniffer.BaseSniffer.BTihItem;
import com.hdx.btlive.sniffer.BaseSniffer.VideoItem;
import com.hdx.btlive.utils.SimpleHttpClient;

@SuppressLint("DefaultLocale")
public class HtpFansFinder extends BaseFinder {

	@SuppressLint("DefaultLocale")
	@Override
	public List<BTXunleiItem> getList(BTihItem p) {
		String url = String
				.format("http://i.vod.xunlei.com/req_subBT/info_hash/%s/req_num/2000/req_offset/0/",
						p.BTih.toUpperCase());
		JSONObject resp = SimpleHttpClient.getJSON(url);
		List<BTXunleiItem> list = new ArrayList<BaseSniffer.BTXunleiItem>();
		try {
			JSONArray subfile_list = resp.getJSONObject("resp").getJSONArray(
					"subfile_list");
			if (subfile_list != null) {
				for (int i = 0; i < subfile_list.length(); i++) {
					JSONObject o = subfile_list.getJSONObject(i);
					BTXunleiItem vi = new BTXunleiItem(o.getString("name"),
							p.BTih.toUpperCase(), o.getString("gcid"),
							o.getString("index"));
					list.add(vi);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public List<VideoItem> getPlayableUrls(BTXunleiItem vi) {
		List<VideoItem> resultList = new ArrayList<BaseSniffer.VideoItem>();
		String url = String.format(
				"http://tv1.hdpfans.com:8088/api/bt/%s/%s/m3u8", vi.cid,vi.index);
		JSONObject resp = SimpleHttpClient.getJSON(url);
		if(resp !=null){
			try {
				JSONArray flvs = resp.getJSONArray("m3u8s");
				if (flvs != null) {
					for (int i = 0; i < flvs.length(); i++) {
						JSONObject o = flvs.getJSONObject(i);
						VideoItem v = new VideoItem();
						v.url = o.getString("vod_url");
						resultList.add(v);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
//		else{};
		return resultList;
	}

}
