package com.hdx.btlive.finder;

import com.hdx.btlive.constants.Constants;

import android.content.Context;


public class FinderFactory {
	private static final String[] FINDER_NAMES = {"迅雷云播", "htp fans", "huoyan.tv", "my own"};
	@SuppressWarnings("rawtypes")
	private static final Class[] FINDER_CLASSES = {XunLeiFinder.class,HtpFansFinder.class,HuoYanTVFindver.class,MyFinder.class};

	public static int getFinderNum() {
		return FINDER_NAMES.length;
	}
	
	public static String getFinderName(int i) {
		if(i < FINDER_NAMES.length && i >= 0) {
			return FINDER_NAMES[i];
		}
		return null;
	}
	
	public static String[] getFinderNames() {
		return FINDER_NAMES;
	}
	
	public static BaseFinder createFinder(int finderIndex) {
		BaseFinder finder = null;
		if(finderIndex < FINDER_NAMES.length && finderIndex >= 0) {
			try {
				finder = (BaseFinder)FINDER_CLASSES[finderIndex].newInstance();
			}
			catch (InstantiationException e) {
				
			}
			catch (IllegalAccessException e) {
				
			}
		}
		return finder;
	}
	public static BaseFinder createFinder(String whichLine){
		BaseFinder finder = null;
		//set finderindex below .
		int finderIndex = 0;
		if(Constants.WHICH_LINE.equals("MyFinder") || Constants.WHICH_LINE == "MyFinder"){
			// My Finder
			finderIndex = 3;
		}else if(Constants.WHICH_LINE.equals("HtpFuns") || Constants.WHICH_LINE == "HtpFuns"){
			// HtpFunsFinder
			finderIndex = 1;
		}else if(Constants.WHICH_LINE.equals("HYTV") || Constants.WHICH_LINE == "HYTV"){
			// HuoYanFinder
			finderIndex = 2;
		}else{
			// default MyFinder
			finderIndex = 3;
		}
		if(finderIndex < FINDER_NAMES.length && finderIndex >= 0) {
			try {
				finder = (BaseFinder)FINDER_CLASSES[finderIndex].newInstance();
			}
			catch (InstantiationException e) {
				
			}
			catch (IllegalAccessException e) {
				
			}
		}
		return finder;
	}
	public static BaseFinder createFinder(Context context) {
		BaseFinder finder = null;
		//set finderindex below .
		int finderIndex = 3;
		if(finderIndex < FINDER_NAMES.length && finderIndex >= 0) {
			try {
				finder = (BaseFinder)FINDER_CLASSES[finderIndex].newInstance();
			}
			catch (InstantiationException e) {
				
			}
			catch (IllegalAccessException e) {
				
			}
		}
		return finder;
	}
}
