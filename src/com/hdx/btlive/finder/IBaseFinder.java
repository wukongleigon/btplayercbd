package com.hdx.btlive.finder;

import java.util.List;

import com.hdx.btlive.sniffer.BaseSniffer.BTXunleiItem;
import com.hdx.btlive.sniffer.BaseSniffer.BTihItem;
import com.hdx.btlive.sniffer.BaseSniffer.VideoItem;

public interface IBaseFinder {
	public  List<BTXunleiItem> getList(BTihItem p) ;
	public  List<VideoItem> getPlayableUrls(BTXunleiItem vi,String btih);
	
}
