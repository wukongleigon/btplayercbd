package com.hdx.btlive.finder;

import java.util.List;

import com.hdx.btlive.sniffer.BaseSniffer.BTXunleiItem;
import com.hdx.btlive.sniffer.BaseSniffer.BTihItem;
import com.hdx.btlive.sniffer.BaseSniffer.VideoItem;

/**
 * 本类实现了从不同途径实现播放影片资源的解决方法。
 * 
 * @author sadieyu
 * 
 */
public abstract class BaseFinder {
	/**
	 * 
	 * @param p
	 * @param sourceDode  資源來源
	 * @return
	 */
	public abstract List<BTXunleiItem> getList(BTihItem p) ;
	public abstract List<VideoItem> getPlayableUrls(BTXunleiItem vi);
	 
}
