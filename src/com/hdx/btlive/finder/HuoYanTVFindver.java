package com.hdx.btlive.finder;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import com.hdx.btlive.sniffer.BaseSniffer;
import com.hdx.btlive.sniffer.BaseSniffer.BTXunleiItem;
import com.hdx.btlive.sniffer.BaseSniffer.BTihItem;
import com.hdx.btlive.sniffer.BaseSniffer.VideoItem;
import com.hdx.btlive.utils.SimpleHttpClient;

public class HuoYanTVFindver extends BaseFinder {

	@SuppressLint("DefaultLocale")
	@Override
	public List<BTXunleiItem> getList(BTihItem p) {
		String btih = p.BTih.toUpperCase();
		String huoyanUrl = String.format(
				"http://www.huoyan.tv/play.php?key=%s", btih);
		String resp = SimpleHttpClient.get(huoyanUrl, "http://www.huoyan.tv");
		List<BTXunleiItem> resultList = new ArrayList<BaseSniffer.BTXunleiItem>();
		if (resp != null) {
			Pattern pattern = Pattern.compile("pcurl=(.+)\"");
			Matcher m = pattern.matcher(resp);
			if (m.find()) {
				String pcurl = m.group(1).trim();
				String realAddr = String.format(
						"http://local.huoyan.tv/v5.php?pcurl=%s", pcurl);
				BTXunleiItem vi = new BTXunleiItem("", realAddr, "","");
				resultList.add(vi);
			}
		}
		return resultList;
	}

	@Override
	public List<VideoItem> getPlayableUrls(BTXunleiItem vi) {
		List<VideoItem> resultList = new ArrayList<BaseSniffer.VideoItem>();
		if (vi != null && !TextUtils.isEmpty(vi.cid)) {
			VideoItem v = new VideoItem();
			v.url = vi.cid;
			resultList.add(v);
		}
		return resultList;
	}

}
