package com.hdx.btlive.finder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.hdx.btlive.sniffer.BaseSniffer;
import com.hdx.btlive.sniffer.BaseSniffer.BTXunleiItem;
import com.hdx.btlive.sniffer.BaseSniffer.BTihItem;
import com.hdx.btlive.sniffer.BaseSniffer.VideoItem;
import com.hdx.btlive.utils.SimpleHttpClient;

public class XunLeiFinder extends BaseFinder {
	protected String uid = "244647876";

	@Override
	public List<BTXunleiItem> getList(BTihItem p) {
		String url = String
				.format("http://i.vod.xunlei.com/req_subBT/info_hash/%s/req_num/2000/req_offset/0/",
						p.BTih.toUpperCase());
		JSONObject resp = SimpleHttpClient.getJSON(url);
		List<BTXunleiItem> list = new ArrayList<BaseSniffer.BTXunleiItem>();
		try {
			JSONArray subfile_list = resp.getJSONObject("resp").getJSONArray(
					"subfile_list");
			if (subfile_list != null) {
				for (int i = 0; i < subfile_list.length(); i++) {
					JSONObject o = subfile_list.getJSONObject(i);
					BTXunleiItem vi = new BTXunleiItem(o.getString("name"),
							o.getString("cid"), o.getString("gcid"),o.getString("index"));
					list.add(vi);
				}
			}
		} catch (JSONException e) {

		}
		return list;
	}

	
	
	@Override
	public List<VideoItem> getPlayableUrls(BTXunleiItem vi) {
		List<VideoItem> resultList = new ArrayList<BaseSniffer.VideoItem>();
		String format = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
				+ "<LixianProtocol Version=\"1.0\">"
				+ "<Command id=\"getplayurl_req\">"
				+ "<user id=\"%s\" name=\"\" newno=\"0\" vip_level=\"5\" net=\"0\" ip=\"1694542016\" key=\"\" from=\"2\"/>"
				+ "<content gcid=\"%s\" cid=\"%s\" dev_width=\"720\" dev_hight=\"1280\" section_type=\"0\" filesize=\"1\" max_fpfile_num=\"10\" video_type=\"7\" />"
				+ "</Command>" + "</LixianProtocol>"; 
		// 随机获取UID号 getRandom();
		String postXML = String.format(format, uid, vi.gcid, vi.cid);
		String resp = SimpleHttpClient.post("http://pad.i.vod.xunlei.com/",
				postXML);
		Document doc = Jsoup.parse(resp);
		Elements command = doc.select("command");
		String[] filetypes = { "play_url", "ts_url", "mp4_url" };
		String[] resolutions = { "high_url", "middle_url", "normal_url" };
		if (command.get(0).attr("message").equals("ok")) {
			for (int i = 0; i < filetypes.length; i++) {
				Elements items = doc.select(filetypes[i]);
				for (int j = 0; j < resolutions.length; j++) {
					String durl = items.get(0).attr(resolutions[j]);
					if (!durl.equals("")) {
						VideoItem v = new VideoItem();
						v.url = durl;
						v.weight = (j + 1) << 8 + (i + 1);
						resultList.add(v);
					}
				}
			}
		}

		Collections.sort(resultList, new Comparator<VideoItem>() {

			@Override
			public int compare(VideoItem lhs, VideoItem rhs) {
				return lhs.weight - rhs.weight;
			}
		});
		return resultList;
	}

}
