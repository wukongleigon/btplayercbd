package com.hdx.btlive.sniffer;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.hdx.btlive.constants.Constants;

import android.app.Activity;
import android.util.Log;

/**
 * DOWNLOAD FROM SC
 * 
 * @author sadieyu
 * 
 */
public class BTScgSniffer extends BaseSniffer {
	Activity mContext;
	String html;

	@Override
	public List<SearchResultItem> searchByKeyword(String keyword, int page) {
		String searchUrl = getSearchUrl(keyword);
		List<SearchResultItem> result = new ArrayList<BaseSniffer.SearchResultItem>();
		if (searchUrl != null) {
			// Log.e("SEARCH_RUL", searchUrl);
			String html = fetchUrl(searchUrl);
			if (html != null) {
				// Log.e("Jsoup_pre", "Jsoup Pre = "+html.substring(0,500));
				Document soup = Jsoup.parse(html);
				// Log.e("Jsoup_pre", "Jsoup Pre = "+soup.data());
				Elements result_items = soup.select("#result-items li");
				// Log.e("Jsoup_pre", "Jsoup After = "+soup.data());
				for (int i = 0; i < result_items.size(); i++) {
					SearchResultItem item = new SearchResultItem();
					Element link = result_items.get(i).select("h3 a").first();
					item.label = link.text();
					item.flag = Constants.SCJY;
					item.url = link.attr("href");
					item.name = link.text();
					result.add(item);
				}
			}

		}
		return result;
	}

	@SuppressWarnings("deprecation")
	private String getSearchUrl(String keyword) {
		String html = fetchUrl("http://www.btscg.com/search.php");
		Pattern p = Pattern.compile("var\\s+myParams\\s*=\\s*\\'(.+)\\'");
		if (html == null) {
			Log.e("dangerous", "html is null in getsearchurl");
		}
		if (null == html) {
			return null;
		}
		Matcher m = p.matcher(html);
		String params = String
				.format("&q=%s&timeLength=0&qs=txt.adv.a&orderField=default&threadScope=all&orderType=desc&myfIds%%5B%%5D=2&myfIds%%5B%%5D=36&searchLevel=4",
						URLEncoder.encode(keyword));
		if (m.find()) {
			String myParam = m.group(1).trim();
			return "http://search.btscg.com/f/search?" + myParam + params;
		}
		return null;
	}

	@Override
	public BTihItem getBTih(SearchResultItem item) {
		BTihItem playableItem = new BTihItem();
		playableItem.name = item.name;
		String html = fetchUrl(item.url);
		if (html != null) {
			Pattern pattern = Pattern
					.compile("href=\"magnet:\\?xt=urn:btih:([0-9a-fA-F]{40})");
			Matcher m = pattern.matcher(html);
			if (m != null) {
				if (m.find()) {
					playableItem.BTih = m.group(1);
				} else {
					Pattern p2 = Pattern
							.compile("href=\"[^\"]*([0-9a-fA-F]{40})\\.torrent");
					m = p2.matcher(html);
					if (m.find()) {
						playableItem.BTih = m.group(1);
					}
				}
			}
		} else {

		}
		return playableItem.BTih == null ? null : playableItem;
	}
}
