package com.hdx.btlive.sniffer;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.hdx.btlive.constants.Constants;

public class SnifferFactory {
	private static final String[] SNIFFER_NAMES = {"圣城家园", "torrentkitty","torrentkittycn","btdigg"};
	@SuppressWarnings("rawtypes")
	private static final Class[] SNIFFER_CLASSES = {BTScgSniffer.class, TorrentkittySniffer.class, TKcnSniffer.class, BtDiggSniffer.class};

	public static int getSnifferNum() {
		return SNIFFER_NAMES.length;
	}
	
	public static String getSnifferName(int i) {
		if(i < SNIFFER_NAMES.length && i >= 0) {
			return SNIFFER_NAMES[i];
		}
		return null;
	}
	
	public static String[] getSnifferNames() {
		return SNIFFER_NAMES;
	}
	
	public static BaseSniffer createSniffer(int snifferIndex) {
		BaseSniffer sniffer = null;
		if(snifferIndex < SNIFFER_NAMES.length && snifferIndex >= 0) {
			try {
				sniffer = (BaseSniffer)SNIFFER_CLASSES[snifferIndex].newInstance();
			}
			catch (InstantiationException e) {
				
			}
			catch (IllegalAccessException e) {
				
			}
		}
		return sniffer;
	}
	
	public static BaseSniffer[] createSnifferArray(int[] snifferIndexes) {
		int len = snifferIndexes.length;
		BaseSniffer[] result = new BaseSniffer[len];
		for (int i = 0; i < len; i++) {
			if(snifferIndexes[i]>=0){
				result[i] = createSniffer(snifferIndexes[i]);
			}
		}
		return result;
	}
	
	public static BaseSniffer createSniffer(Context context) {
		BaseSniffer sniffer = null;
		//默认生成家园搜索    
		int snifferIndex = 0;
		if(Constants.isSetTk){
			//0表示 圣城家园  1表示 torrentKitty
			snifferIndex = 1;
//			HashMap<String,String> map = new HashMap<String, String>();
//			map.put("line1", "TKCN");
//			MobclickAgent.onEvent(context, "source_from",map);
		}else if(Constants.isSetBtscg){
			snifferIndex = 0;
//			HashMap<String,String> map = new HashMap<String, String>();
//			map.put("line2", "BTSCG");
//			MobclickAgent.onEvent(context, "source_from",map);
		}else{
			snifferIndex = 0;
		}
		
 		if(snifferIndex < SNIFFER_NAMES.length && snifferIndex >= 0) {
			try {
				sniffer = (BaseSniffer)SNIFFER_CLASSES[snifferIndex].newInstance();
			}
			catch (InstantiationException e) {
				
			}
			catch (IllegalAccessException e) {
				
			}
		}
		return sniffer;
	}
}
