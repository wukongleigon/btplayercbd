package com.hdx.btlive.sniffer;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.hdx.btlive.constants.Constants;

import android.util.Log;

/**
 * DOWNLOAD FROM SC
 * 
 * @author sadieyu
 * 
 */
public class TKcnSniffer extends BaseSniffer {

	private static final String SEARCH_URL = "http://torrentkittycn.com/search?q=%s";

	@Override
	public List<SearchResultItem> searchByKeyword(String keyword, int page) {
		@SuppressWarnings("deprecation")
		String searchUrl = String.format(SEARCH_URL, URLEncoder.encode(keyword));
		List<SearchResultItem> result = new ArrayList<BaseSniffer.SearchResultItem>();
		String html = fetchUrl(searchUrl);
		if(null != html){
			//解析XML数据  
			Document soup = Jsoup.parse(html);
			Elements result_items = soup.select(".list dl");
			
			for(int i = 0; i<result_items.size(); i++){
				SearchResultItem item = new SearchResultItem();
				Element link = result_items.get(i).select("dt a").first();
				item.label = link.text();
				item.url =link.attr("href");
				item.name =link.text();
				item.flag = Constants.TKCN;
				result.add(item);
			}
		}
		/*
		Document soup = Jsoup.parse(html);
		Elements result_items = soup.select(".action");
		for (int i = 0; i < result_items.size(); i++) {
			SearchResultItem item = new SearchResultItem();
			Elements curr = result_items.get(i).select("a");
			if (curr.size() > 2) {
				Element link = curr.get(1);
				item.label = link.attr("title");
				item.url = link.attr("href");
				item.name = link.attr("title");
				result.add(item);
			}
		}
		*/
		return result;
	}

	@Override
	public BTihItem getBTih(SearchResultItem item) {
//		item.url  =  /show/infohash/1ACA312B582D07D8B78F911E8F2E3FBE93DD137B/nospider
		String magnet = item.url;
		
		BTihItem playableItem = new BTihItem();
		playableItem.name = item.name;
		Pattern pattern = Pattern.compile("/show/infohash/([0-9a-fA-F]{40})");
		Matcher m = pattern.matcher(magnet);
		if (m.find()) {
			playableItem.BTih = m.group(1);
		}
		return playableItem.BTih == null ? null : playableItem;
	}
}
