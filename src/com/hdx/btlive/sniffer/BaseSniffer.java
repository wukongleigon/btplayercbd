package com.hdx.btlive.sniffer;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;

import com.hdx.btlive.utils.SimpleHttpClient;

/**
 * 
 * @author sadieyu
 * 
 */

@SuppressLint("DefaultLocale")
public abstract class BaseSniffer  {

	static String uid = "";
	protected static String[] uids = { "300667269", "244647876" };

	private static String getRandom() {
		// 产生0-(strs.length-1)的整数值,也是数组的索引
		int index = (int) (Math.random() * uids.length);
		uid = uids[index];
		System.out.println("UID = " + uid);
		return uid;
	}
    /**
     * 搜索结果
     * @author sadieyu
     *
     */
	public static class SearchResultItem {
		public String label;
		public String url;
		public String name;
		//用于判断资源来自那个网站
		public int  flag;
	}
    /**
     * 磁力首码
     * @author sadieyu
     *
     */
	public static class BTihItem {
		public String name;
		public String BTih;
	}
    /**
     * BT迅雷
     * @author sadieyu
     *
     */
	public static class BTXunleiItem {
		public String name;
		public String cid;
		public String gcid;
		public String index;
        /**
         * 
         * @param n   name 
         * @param c   cid
         * @param g   gcid
         */
		public BTXunleiItem(String n, String c, String g, String i) {
			name = n;
			cid = c;
			gcid = g;
			index = i ;
		}

		public String toString() {
			return name;
		}
	}

	public static class PlayerablePackage {
		public PlayerablePackage(ArrayList<String> play, ArrayList<String> sub,
				String t) {
			play_uris = play;
			subtitle_uris = sub;
			title = t;
		}

		public ArrayList<String> play_uris;
		public ArrayList<String> subtitle_uris;
		public String title;
	}

	public static class VideoItem {
		public static int CATEGORY_PLAY = 0x3;
		public static int CATEGORY_TS = 0x1;
		public static int CATEGORY_MP4 = 0x2;
		public static int RESOLUTION_HIGH = 0x300;
		public static int RESOLUTION_MIDDLE = 0x200;
		public static int RESOLUTION_NORMAL = 0x100;
		public static int RESPONSE_MESSAGE_ERROR ;

		public int weight;
		public String url;
		public String errorMsg;
	}

	/**
	 * 获取资源列表来源ID代码
	 * 
	 * @author sadieyu
	 * 
	 */
	public static class SourceCode {
		public static final int XUN_LEI_YUN_BO = 0x001;
		public static final int HUO_YAN_TV = 0x002;
	}
    //关键词搜索
	public abstract List<SearchResultItem> searchByKeyword(String keyword,
			int page);

	// GetBTih
	public abstract BTihItem getBTih(SearchResultItem item);
	
	protected String fetchUrl(String url) {
		return SimpleHttpClient.get(url);
	}


	public ArrayList<String> getSubtitleUrl(BTXunleiItem bi) {
		getRandom();
		JSONObject resp = SimpleHttpClient
				.getJSON(String
						.format("http://i.vod.xunlei.com/subtitle/list?cid=%s&gcid=%s&userid=%s&t=%s",
								bi.cid, bi.gcid, uid,
								System.currentTimeMillis()));
		ArrayList<String> ret = new ArrayList<String>();
		try {
			if (resp.getInt("ret") == 0) {
				// return resp.getJSONObject("subtitle").getString("surl");
				JSONArray sublist = resp.getJSONArray("sublist");
				for (int i = 0; i < sublist.length(); i++) {
					if (sublist.getJSONObject(i).getString("sname")
							.endsWith(".ass")) {
						continue;
					}
					ret.add(sublist.getJSONObject(i).getString("surl"));
				}
			}
		} catch (Exception e) {

		}
		return ret;
	}

}
