package com.hdx.btlive.sniffer;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.hdx.btlive.constants.Constants;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * DOWNFROM TORRENT_KITTY
 * 
 * @author sadieyu
 * 
 */
public class BtDiggSniffer extends BaseSniffer {
	Context  context;

	private static final String SEARCH_URL = "http://btdigg.org/search?info_hash=&q=%s";

	@Override
	public List<SearchResultItem> searchByKeyword(String keyword, int page) {
		@SuppressWarnings("deprecation")
		String searchUrl = String
				.format(SEARCH_URL, URLEncoder.encode(keyword));
		List<SearchResultItem> result = new ArrayList<BaseSniffer.SearchResultItem>();
		String html = fetchUrl(searchUrl);
		if (html != null) {
			Document soup = Jsoup.parse(html);
			Elements result_items = soup.select("td.torrent_name > a");
			int resultSize = result_items.size();
			Log.v("BTDIGG_RESULT", "STOP");
			for (int i = 0; i < resultSize; i++) {
				SearchResultItem item = new SearchResultItem();
				Element link = result_items.get(i);
				item.label = link.text();
				item.name = link.text();
				item.flag = Constants.BTDIGG;
				item.url = link.attr("href");
				result.add(item);
			}
		}
		return result;
	}

	@Override
	public BTihItem getBTih(SearchResultItem item) {
		String magnet = item.url;
		BTihItem playableItem = new BTihItem();
		playableItem.name = item.name;
		// /search?info_hash=
		playableItem.BTih = magnet.substring(18, 58);
		/*
		 * Pattern pattern =
		 * Pattern.compile("/search?info_hash=([0-9a-fA-F]{40})"); Matcher m =
		 * pattern.matcher(magnet); if (m.find()) { playableItem.BTih =
		 * m.group(1).toUpperCase(); Log.v("BTDIGG_BTIH", "STOP"); }
		 */
		return playableItem.BTih == null ? null : playableItem;
	}

}
