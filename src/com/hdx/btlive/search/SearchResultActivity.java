package com.hdx.btlive.search;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import cn.waps.AdView;
import cn.waps.AppConnect;

import com.hdx.btlive.R;
import com.hdx.btlive.constants.Constants;
import com.hdx.btlive.finder.BaseFinder;
import com.hdx.btlive.finder.HtpFansFinder;
import com.hdx.btlive.finder.HuoYanTVFindver;
import com.hdx.btlive.finder.MyFinder;
import com.hdx.btlive.player.VideoViewPlayingActivity;
import com.hdx.btlive.sniffer.BaseSniffer;
import com.hdx.btlive.sniffer.BaseSniffer.BTXunleiItem;
import com.hdx.btlive.sniffer.BaseSniffer.BTihItem;
import com.hdx.btlive.sniffer.BaseSniffer.PlayerablePackage;
import com.hdx.btlive.sniffer.BaseSniffer.SearchResultItem;
import com.hdx.btlive.sniffer.BaseSniffer.VideoItem;
import com.hdx.btlive.sniffer.SnifferFactory;
import com.hdx.btlive.sniffer.TKcnSniffer;
import com.hdx.btlive.ui.BaseActivity;
import com.hdx.btlive.ui.HomeFragment;
import com.hdx.btlive.ui.MainFragmentActivity;
import com.hdx.btlive.ui.selfdialog.CustomProgressDialog;
import com.umeng.analytics.MobclickAgent;

public class SearchResultActivity extends BaseActivity implements
		OnItemClickListener, OnScrollListener {

	private static final String TAG = "DEBUG-TAG";

	public static final String SEARCH_KEYWORD = "com.wukongtv.cinema.search_activity.keyword";
	public static final String SEARCH_ENGINE_ID = "com.wukongtv.cinema.search_activity.search_engine";

	boolean hasPopAd;
	AsyncTask<SearchResultItem, Integer, PlayerablePackage> exploreItem;
	List<SearchResultItem> listResutlItems;

	String wrokingMessage;
	String keyword;
	SharedPreferences settingShare;
	AsyncTask<String, Integer, List<SearchResultItem>> btscgTask, tkTask,
			tkcnTask, btdiggTask;

	// private String cbpKey = null;
	private String btscgKey = null;
	private String torrentKey = null;
	private String lpKey = null;
	private String btdiggKey = null;

	ProgressDialog mProgressDialog;
	CustomProgressDialog progressDialog;
	List<SearchResultItem> listAdapter;
	// public static final int TKCN = 0x001;
	// public static final int SCJY = 0x002;

	ListView mSearchResults;
	SearchResultAdatper mSearchAdapter;
	// BaseSniffer mSnifferBT,mSnifferTK;
	// BaseSniffer[] mSniffer;
	BaseSniffer mSniffer;
	BaseSniffer[] mSniffers;
	// mSnifferTK mSnifferBTSCG;
	List<BaseSniffer> listSniffer;
	// = new ArrayList<BaseSniffer>();
	BaseFinder mFinder;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.searchlist);

		// 广告
		LinearLayout container = (LinearLayout) findViewById(R.id.AdLinearLayout);
		new AdView(this, container).DisplayAd();
		// 初始化插屏广告数据
		AppConnect.getInstance(this).initPopAd(this);

		mSearchResults = (ListView) findViewById(R.id.lv_search_list);
		torrentKey = getResources().getString(R.string.c_torrentkitty);
		btscgKey = getResources().getString(R.string.c_btscg);
		lpKey = getResources().getString(R.string.lp_key);
		btdiggKey = getResources().getString(R.string.c_btdigg);

		mSearchAdapter = new SearchResultAdatper(this);

		mSearchResults.setAdapter(mSearchAdapter);

		mSearchResults.setOnItemClickListener(this);
		mSearchResults.setOnScrollListener(this);
		MobclickAgent.onEvent(SearchResultActivity.this,
				"SearchResultItemClick");
		keyword = getIntent().getStringExtra(SEARCH_KEYWORD);
		/**
		 * Set source Line
		 */
		settingShare = getSharedPreferences("setting", MODE_PRIVATE);

		if (settingShare.getString("WHICH_LINE", "HtpFuns").equals("MyFinder")
				|| settingShare.getString("WHICH_LINE", "MyFinder") == "MyFinder") {
			mFinder = new MyFinder(SearchResultActivity.this);
		} else if (settingShare.getString("WHICH_LINE", "MyFinder").equals(
				"HtpFuns")
				|| settingShare.getString("WHICH_LINE", "MyFinder") == "HtpFuns") {
			mFinder = new HtpFansFinder();
		} else if (settingShare.getString("WHICH_LINE", "MyFinder").equals(
				"HYTV")
				|| settingShare.getString("WHICH_LINE", "MyFinder") == "HYTV") {
			mFinder = new HuoYanTVFindver();
		} else {
			mFinder = new HtpFansFinder();
		}

		startProgressDialog(true);
		progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface dialog) {
				// TODO Auto-generated method stub
				Message msg = new Message();
				msg.what = Constants.CANCLE_AND_INTENT_HOME;
				mHandler.sendMessage(msg);
				
			   if(null != btscgTask && btscgTask.getStatus()!=Status.FINISHED){
				   btscgTask.cancel(true);
			   }	
			   if(null!=tkTask && tkTask.getStatus()!=Status.FINISHED){
				   tkTask.cancel(true);
			   }	
			   if(null!=tkcnTask && tkcnTask.getStatus()!=Status.FINISHED){
				   tkcnTask.cancel(true);
			   }	
			   if(null !=btdiggTask && btdiggTask.getStatus()!=Status.FINISHED){
				   btdiggTask.cancel(true);
			   }	
			}
		});
		
		// progressDialog.setWorkingMessage(mSearchAdapter.getmSearchResults().toString());

		/**
		 * 设置播放线路 mSniffer
		 */
		chooseVideoSource();

	}

	private void startProgressDialog(final boolean setCancelable) {
		if (progressDialog == null) {
			progressDialog = CustomProgressDialog.createDialog(this);
			progressDialog.setMessage("正在挣命加载中...");
			progressDialog.setCancelable(setCancelable);
		}

		progressDialog.show();
	}

	private void stopProgressDialog() {
		if (progressDialog != null) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	/**
	 * 用于监听插屏广告的显示与关闭
	 */
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		Dialog dialog = AppConnect.getInstance(this).getPopAdDialog();
		if (dialog != null) {
			if (dialog.isShowing()) {
				// 插屏广告正在显示
			}
			dialog.setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					// 监听插屏广告关闭事件
				}
			});
		}
	}

	/**
	 * 设置线路优化方法
	 */
	protected void chooseSource() {
		boolean isSetBtscg, isSetTk, isSetTKCN, isSetBtdigg, isSetOabt;
		isSetBtscg = settingShare.getBoolean("IS_SET_BTSCG", false);
		isSetTk = settingShare.getBoolean("IS_SET_TK", false);
		isSetTKCN = settingShare.getBoolean("IS_SET_TKCN", false);
		isSetBtdigg = settingShare.getBoolean("IS_SET_BTDIGG", true);
		// 长度为的数组 存放搜索资源网站
		int[] sourceSniffer = { -1, -1, -1, -1 };
		if (isSetBtscg == true) {
			sourceSniffer[0] = 0;
		}
		if (isSetTk == true) {
			sourceSniffer[1] = 1;
		}
		if (isSetTKCN == true) {
			sourceSniffer[2] = 2;
		}
		if (isSetBtdigg == true) {
			sourceSniffer[3] = 3;
		}
		mSniffers = SnifferFactory.createSnifferArray(sourceSniffer);
	}

	/**
	 * 设置播放线路
	 */

	protected void chooseVideoSource() {
		// TODO Auto-generated method stub
		boolean isSetBtscg, isSetTk, isSetTKCN, isSetBtdigg, isSetOabt;
		isSetBtscg = settingShare.getBoolean("IS_SET_BTSCG", true);
		isSetTk = settingShare.getBoolean("IS_SET_TK", false);
		isSetTKCN = settingShare.getBoolean("IS_SET_TKCN", false);
		isSetBtdigg = settingShare.getBoolean("IS_SET_BTDIGG", false);

		// oabt 功能暂未开放
		// isSetOabt = settingShare.getBoolean("IS_SET_OABT", false);
		if (isSetBtscg == true && isSetTk == false && isSetTKCN == false
				&& isSetBtdigg == false) {
			// only BTSCG
			mSniffer = SnifferFactory.createSniffer(0);
			btscgTask = new SearchTaskBtscg().execute(keyword);

			Log.v("SEARCH_TASK", "BTSCG_ONLY");

		} else if (isSetTk == true && isSetBtscg == false && isSetTKCN == false
				&& isSetBtdigg == false) {
			// only TK
			mSniffer = SnifferFactory.createSniffer(1);
			tkTask = new SearchTaskTK().execute(keyword);

			Log.v("SEARCH_TASK", "TK_ONLY");

		} else if (isSetTKCN == true && isSetBtscg == false && isSetTk == false
				&& isSetBtdigg == false) {
			// only TKCN
			mSniffer = SnifferFactory.createSniffer(2);
			tkcnTask = new SearchTaskTKCN().execute(keyword);

			Log.v("SEARCH_TASK", "TKCN_ONLY");

		} else if (isSetBtdigg == true && isSetBtscg == false
				&& isSetTk == false && isSetTKCN == false) {
			// only BTDIGG
			mSniffer = SnifferFactory.createSniffer(3);
			btdiggTask = new SearchTaskBtDigg().execute(keyword);

			Log.v("SEARCH_TASK", "BTDIGG_ONLY");

		} else if (isSetBtscg == true && isSetTk == true && isSetTKCN == false
				&& isSetBtdigg == false) {
			// BTSCG TK
			mSniffer = SnifferFactory.createSniffer(1);
			tkTask = new SearchTaskTK().execute(keyword);

			mSniffer = SnifferFactory.createSniffer(0);
			btscgTask = new SearchTaskBtscg().execute(keyword);

			Log.v("SEARCH_TASK", "BTSCG_TK");

		} else if (isSetBtscg == true && isSetTKCN == true && isSetTk == false
				&& isSetBtdigg == false) {
			// BTSCG TKCN
			mSniffer = SnifferFactory.createSniffer(0);
			btscgTask = new SearchTaskBtscg().execute(keyword);

			mSniffer = SnifferFactory.createSniffer(2);
			tkcnTask = new SearchTaskTKCN().execute(keyword);

			Log.v("SEARCH_TASK", "BTSCG_TKCN");

		} else if (isSetBtscg == true && isSetBtdigg == true
				&& isSetTk == false && isSetTKCN == false) {
			// BTSCG BTDIGG
			mSniffer = SnifferFactory.createSniffer(0);
			btscgTask = new SearchTaskBtscg().execute(keyword);

			mSniffer = SnifferFactory.createSniffer(3);
			btdiggTask = new SearchTaskBtDigg().execute(keyword);

			Log.v("SEARCH_TASK", "BTSCG_BTDIGG");

		} else if (isSetTk == true && isSetTKCN == true && isSetBtscg == false
				&& isSetBtdigg == false) {
			// TK TKCN
			mSniffer = SnifferFactory.createSniffer(1);
			tkTask = new SearchTaskTK().execute(keyword);

			mSniffer = SnifferFactory.createSniffer(2);
			tkcnTask = new SearchTaskTKCN().execute(keyword);

			Log.v("SEARCH_TASK", "TK_TKCN");

		} else if (isSetTk == true && isSetBtdigg == true
				&& isSetBtscg == false && isSetTKCN == false) {
			// TK BTDIGG  #####################################################
			mSniffer = SnifferFactory.createSniffer(1);
			tkTask = new SearchTaskTK().execute(keyword);

			mSniffer = SnifferFactory.createSniffer(3);
			btdiggTask = new SearchTaskBtDigg().execute(keyword);

			Log.v("SEARCH_TASK", "TK_BTDIGG");

		} else if (isSetTKCN == true && isSetBtdigg == true
				&& isSetBtscg == false && isSetTk == false) {
			// TKCN BTDIGG
			mSniffer = SnifferFactory.createSniffer(2);
			tkcnTask = new SearchTaskTKCN().execute(keyword);

			mSniffer = SnifferFactory.createSniffer(3);
			btdiggTask = new SearchTaskBtDigg().execute(keyword);

			Log.v("SEARCH_TASK", "BTDIGG_TKCN");

		} else if (isSetBtscg == true && isSetTk == true && isSetTKCN == true
				&& isSetBtdigg == false) {
			// BTSCG TK TKCN
			mSniffer = SnifferFactory.createSniffer(1);
			tkTask = new SearchTaskTK().execute(keyword);

			mSniffer = SnifferFactory.createSniffer(0);
			btscgTask = new SearchTaskBtscg().execute(keyword);

			mSniffer = SnifferFactory.createSniffer(2);
			tkcnTask = new SearchTaskTKCN().execute(keyword);

			Log.v("SEARCH_TASK", "BTSCG_TK_TKCN");

		} else if (isSetBtscg == true && isSetTk == true && isSetBtdigg == true
				&& isSetTKCN == false) {
			// BTSCG TK BTDIGG
			mSniffer = SnifferFactory.createSniffer(1);
			tkTask = new SearchTaskTK().execute(keyword);

			mSniffer = SnifferFactory.createSniffer(0);
			btscgTask = new SearchTaskBtscg().execute(keyword);

			mSniffer = SnifferFactory.createSniffer(3);
			btdiggTask = new SearchTaskBtDigg().execute(keyword);

			Log.v("SEARCH_TASK", "BTSCG_TK_BTDIGG");

		} else if (isSetBtscg == true && isSetTKCN == true
				&& isSetBtdigg == true && isSetTk == false) {
			// BTSCG TKCN BTDIGG

			mSniffer = SnifferFactory.createSniffer(2);
			tkcnTask = new SearchTaskTKCN().execute(keyword);

			mSniffer = SnifferFactory.createSniffer(0);
			btscgTask = new SearchTaskBtscg().execute(keyword);

			mSniffer = SnifferFactory.createSniffer(3);
			btdiggTask = new SearchTaskBtDigg().execute(keyword);

			Log.v("SEARCH_TASK", "BTSCG_TKCN_BTDIGG");

		} else if (isSetTk == true && isSetTKCN == true && isSetBtdigg == true
				&& isSetBtscg == false) {
			// TK TKCN BTDIGG
			mSniffer = SnifferFactory.createSniffer(1);
			tkTask = new SearchTaskTK().execute(keyword);

			mSniffer = SnifferFactory.createSniffer(0);
 			btscgTask = new SearchTaskBtscg().execute(keyword);

			mSniffer = SnifferFactory.createSniffer(3);
			btdiggTask = new SearchTaskBtDigg().execute(keyword);

			Log.v("SEARCH_TASK", "TK_TKCN_BTDIGG");

		} else if (isSetTk == true && isSetTKCN == true && isSetBtdigg == true
				&& isSetBtscg == true) {
			// TK TKCN BTDIGG BTSCG
			mSniffer = SnifferFactory.createSniffer(0);
			btscgTask = new SearchTaskBtscg().execute(keyword);

			mSniffer = SnifferFactory.createSniffer(1);
			tkTask = new SearchTaskTK().execute(keyword);

			mSniffer = SnifferFactory.createSniffer(2);
			tkcnTask = new SearchTaskTKCN().execute(keyword);

			mSniffer = SnifferFactory.createSniffer(3);
			btdiggTask = new SearchTaskBtDigg().execute(keyword);

			Log.v("SEARCH_TASK", "BTSCG_TK_TKCN_BTDIGG");

		} else {
			mSniffer = SnifferFactory.createSniffer(3);
			btdiggTask = new SearchTaskBtscg().execute(keyword);

			Log.v("SEARCH_TASK", "BTDIGG_ELSE");

		}
	}

	public Handler mHandler = new Handler() {

		public void handleMessage(Message msg) {

			switch (msg.what) {
			case Constants.BTSCG_OK:
				Constants.BT_RESULT_OK = true;
				// mProgressDialog.dismiss();
				stopProgressDialog();
				// 插屏广告
				hasPopAd = AppConnect.getInstance(SearchResultActivity.this)
						.hasPopAd(SearchResultActivity.this);
				if (hasPopAd) {
					AppConnect.getInstance(SearchResultActivity.this)
							.showPopAd(SearchResultActivity.this);
					// 根据指定的theme样式展示插屏广告，theme主要为系统样式id
					// AppConnect.getInstance(this).showPopAd(this,
					// android.R.style.Theme_Translucent);
				}
				// 插屏广告
				break;
			case Constants.TKCN_OK:
				Constants.TK_RESULT_OK = true;
				// mProgressDialog.dismiss();
				stopProgressDialog();
				// 插屏广告
				hasPopAd = AppConnect.getInstance(SearchResultActivity.this)
						.hasPopAd(SearchResultActivity.this);
				if (hasPopAd) {
					AppConnect.getInstance(SearchResultActivity.this)
							.showPopAd(SearchResultActivity.this);
					// 根据指定的theme样式展示插屏广告，theme主要为系统样式id
					// AppConnect.getInstance(this).showPopAd(this,
					// android.R.style.Theme_Translucent);
				}
				break;
			case Constants.TK_OK:
				Constants.TKCN_RESULT_OK = true;
				// mProgressDialog.dismiss();
				stopProgressDialog();
				// 插屏广告
				hasPopAd = AppConnect.getInstance(SearchResultActivity.this)
						.hasPopAd(SearchResultActivity.this);
				if (hasPopAd) {
					AppConnect.getInstance(SearchResultActivity.this)
							.showPopAd(SearchResultActivity.this);
					// 根据指定的theme样式展示插屏广告，theme主要为系统样式id
					// AppConnect.getInstance(this).showPopAd(this,
					// android.R.style.Theme_Translucent);
				}
				break;
			case Constants.BTDIGG:
				Constants.BTDIGG_RESULT_OK = true;
				// mProgressDialog.dismiss();
				stopProgressDialog();
				// 插屏广告
				hasPopAd = AppConnect.getInstance(SearchResultActivity.this)
						.hasPopAd(SearchResultActivity.this);
				if (hasPopAd) {
					AppConnect.getInstance(SearchResultActivity.this)
							.showPopAd(SearchResultActivity.this);
					// 根据指定的theme样式展示插屏广告，theme主要为系统样式id
					// AppConnect.getInstance(this).showPopAd(this,
					// android.R.style.Theme_Translucent);
				}
				break;
			case Constants.MESSAGE_ERROR:
				stopProgressDialog();
				Toast.makeText(SearchResultActivity.this, "未找到该资源的播放地址，请尝试其他。",
						Toast.LENGTH_SHORT).show();

				finish();
				break;
			case Constants.CANCLE_AND_INTENT_HOME:
				Intent intent = new Intent(SearchResultActivity.this,MainFragmentActivity.class);
				startActivity(intent);
				finish();
				break;
			case Constants.CANCLE_GET_PLAYABLE_URL:
				Toast.makeText(SearchResultActivity.this, "怎么取消播放了尼～", Toast.LENGTH_SHORT).show();
				break;
			default:
				break;
			}
		}

	};

	public void onPause() {
		super.onPause();
		// mProgressDialog.dismiss();
		stopProgressDialog();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (exploreItem != null
				&& exploreItem.getStatus() != AsyncTask.Status.FINISHED) {
			exploreItem.cancel(true);
			Log.v("AsyncTask", "AsyncTask Finish in ON_DESTROY .");
		}
		AppConnect.getInstance(SearchResultActivity.this).close();
	}

	/**
	 * 获取播放链接
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		SearchResultItem item = (SearchResultItem) mSearchAdapter
				.getItem(position);
		exploreItem = new ExplorerTask().execute(item);
		Constants.isInExploreTask = false;

	}

	/**
	 * 请求TorrentKitty
	 * 
	 * @author sadieyu
	 * 
	 */
	private class SearchTaskTK extends
			AsyncTask<String, Integer, List<SearchResultItem>> {
		
		@Override
		protected List<SearchResultItem> doInBackground(String... params) {
			Log.v("IN-TK","IN-TK");
			return mSniffer.searchByKeyword(params[0], 0);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(List<SearchResultItem> result) {

			if (result.size() == 0) {
				//
				// finish();
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("no_search_result", "没有找到搜索资源或者该资源网站暂不可用");
				MobclickAgent.onEvent(SearchResultActivity.this,
						"search_result_null", map);
				Toast.makeText(SearchResultActivity.this,
						"抱歉没有找到资源。请在更多中设置搜索资源和线路。", Toast.LENGTH_LONG).show();
			} else {
				mSearchAdapter.setSearchResults(result, Constants.TK);
				
				//##############################################################
				
				Log.v("TK_RESULT", result.toString());
				//Debug code below 
				if(result.size()!=0){
					for(int i = 0 ; i<result.size(); i++){
						Log.v("TK_RESULT_ITEM", result.get(i).name);
					}
				}
				
			}

			// 发送消息
			Message message = new Message();
			message.what = Constants.TK_OK;
			mHandler.sendMessage(message);

		}

	}

	/**
	 * 关键词 请求 TorrentKittyCN
	 * 
	 * @author sadieyu
	 * 
	 */
	private class SearchTaskTKCN extends
			AsyncTask<String, Integer, List<SearchResultItem>> {

		@Override
		protected List<SearchResultItem> doInBackground(String... params) {
			return mSniffer.searchByKeyword(params[0], 0);
		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(List<SearchResultItem> result) {

			if (result.size() == 0) {
				//
				// finish();
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("no_search_result", "没有找到搜索资源或者该资源网站暂不可用");
				MobclickAgent.onEvent(SearchResultActivity.this,
						"search_result_null", map);
				Toast.makeText(SearchResultActivity.this,
						"抱歉没有找到资源。请在更多中设置搜索资源和线路。", Toast.LENGTH_LONG).show();
			} else {
				mSearchAdapter.setSearchResults(result, Constants.TKCN);
			}

			// 发送消息
			Message message = new Message();
			message.what = Constants.TKCN_OK;
			mHandler.sendMessage(message);

		}

	}

	/**
	 * 关键词 请求 BtDigg
	 * 
	 * @author sadieyu
	 * 
	 */
	private class SearchTaskBtDigg extends
			AsyncTask<String, Integer, List<SearchResultItem>> {

		@Override
		protected List<SearchResultItem> doInBackground(String... params) {
			Log.v("IN-BTDIGG","IN_BTDIGG");
			return mSniffer.searchByKeyword(params[0], 0);
		}

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(List<SearchResultItem> result) {

			if (result.size() == 0) {
				//
				// finish();
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("no_search_result", "没有找到搜索资源或者该资源网站暂不可用");
				MobclickAgent.onEvent(SearchResultActivity.this,
						"search_result_null", map);
				Toast.makeText(SearchResultActivity.this,
						"抱歉没有找到资源。请在更多中设置搜索资源和线路。", Toast.LENGTH_LONG).show();
			} else {
				mSearchAdapter.setSearchResults(result, Constants.BTDIGG);
				
				//##############################################################
				Log.v("BTDIGG_RESULT", result.toString());
				if(result.size()!=0){
					for(int i = 0 ; i<result.size(); i++){
						Log.v("TK_RESULT_ITEM", result.get(i).name);
					}
				}
				
			}

			Message message = new Message();
			message.what = Constants.BTDIGG;
			mHandler.sendMessage(message);

		}

	}

	/**
	 * 
	 * @author sadieyu
	 * 
	 */
	private class SearchTaskBtscg extends
			AsyncTask<String, Integer, List<SearchResultItem>> {

		@Override
		protected List<SearchResultItem> doInBackground(String... params) {
			return mSniffer.searchByKeyword(params[0], 0);
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(List<SearchResultItem> result) {

			if (result.size() == 0) {
				
				Log.v("TK_RESULT", result.toString());
				
				// finish();
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("no_search_result", "没有找到搜索资源或者该资源网站暂不可用");
				MobclickAgent.onEvent(SearchResultActivity.this,
						"search_result_null", map);
				Toast.makeText(SearchResultActivity.this,
						"抱歉没有找到资源。请在更多中设置搜索资源和线路。", Toast.LENGTH_LONG).show();
			} else {
				mSearchAdapter.setSearchResults(result, Constants.SCJY);
			}

			Message message = new Message();
			message.what = Constants.BTSCG_OK;
			mHandler.sendMessage(message);

		}

	}

	/**
	 * 
	 * @author sadieyu
	 * 
	 */
	public class ExplorerTask extends
			AsyncTask<SearchResultItem, Integer, PlayerablePackage> {
		// ProgressDialog mProgressDialog;
		@Override
		protected void onPreExecute() {
			// mProgressDialog = new ProgressDialog(SearchResultActivity.this);
			// mProgressDialog.setMessage(" 视频加载中...");
			// mProgressDialog.setCanceledOnTouchOutside(false);
			// mProgressDialog.show();
			Constants.isInExploreTask = true;
			startProgressDialog(true);
			progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
				
				@Override
				public void onCancel(DialogInterface dialog) {
					
					Message msg = new Message();
					msg.what = Constants.CANCLE_GET_PLAYABLE_URL;
					mHandler.sendMessage(msg);
					// TODO Auto-generated method stub
					exploreItem.cancel(true);
				}
			});
			
			super.onPreExecute();
		}

		@Override
		protected PlayerablePackage doInBackground(SearchResultItem... params) {
			BTihItem btih = mSniffer.getBTih(params[0]);
			if (btih != null) {
				List<BTXunleiItem> btitems = mFinder.getList(btih);

				if (btitems.size() > 0) {
					List<VideoItem> vis = mFinder.getPlayableUrls(btitems
							.get(0));
					// 获取playableURL ！= 0
					if (vis.size() > 0) {
						if (vis.get(0).url
								.equals(Constants.RESPONSE_MESSAGE_ERROR)) {
							Message msg = new Message();
							msg.what = Constants.MESSAGE_ERROR;
							mHandler.sendMessage(msg);
						} else {
							ArrayList<String> subtitle_url = mSniffer
									.getSubtitleUrl(btitems.get(0));
							ArrayList<String> uris = new ArrayList<String>();
							for (int i = 0; i < vis.size(); i++) {
								uris.add(vis.get(i).url);
							}
							if (vis.size() > 0) {
								String title = "";
								try {
									title = URLDecoder.decode(
											btitems.get(0).name, "utf8");
								} catch (UnsupportedEncodingException e) {

								}
								return new PlayerablePackage(uris,
										subtitle_url, title);
							}
						}
					} else {
						// Playable == 0
						Message msg = new Message();
						msg.what = Constants.MESSAGE_ERROR;
						mHandler.sendMessage(msg);
					}
				} else {
					Message msg = new Message();
					msg.what = Constants.MESSAGE_ERROR;
					mHandler.sendMessage(msg);
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(PlayerablePackage result) {
			// mProgressDialog.dismiss();
			stopProgressDialog();

			if (result != null) {
				Intent i = new Intent(SearchResultActivity.this,
						VideoViewPlayingActivity.class);
				i.putExtra(VideoViewPlayingActivity.PLAY_URI, result.play_uris);
				i.putExtra(VideoViewPlayingActivity.SUBTITLE_URI,
						result.subtitle_uris);
				i.putExtra(VideoViewPlayingActivity.PLAY_TITLE, result.title);
				startActivity(i);
			} else {
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("video_cannot_paly", "视频无法播放");
				MobclickAgent.onEvent(SearchResultActivity.this,
						"can_not_play", map);
				Toast.makeText(SearchResultActivity.this, "该视频无法播放",
						Toast.LENGTH_SHORT).show();

			}
		}

	}

	/**
	 * 加载更多条目
	 */

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub

	}
}
