package com.hdx.btlive.search;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hdx.btlive.R;
import com.hdx.btlive.constants.Constants;
import com.hdx.btlive.sniffer.BaseSniffer.SearchResultItem;

public class SearchResultAdatper extends BaseAdapter {

	Activity mContext;

	Vector<SearchResultItem> mSearchResults;
	List<SearchResultItem> mSearchResults1;

	public List<SearchResultItem> getmSearchResults() {
		return mSearchResults1;
	}

	public Vector<SearchResultItem> getSearchResultss() {
		return mSearchResults;
	}

	public SearchResultAdatper(Activity context) {
		mContext = context;
		mSearchResults1 = new ArrayList<SearchResultItem>();
		mSearchResults = new Vector<SearchResultItem>();
	}

	public void setSearchResults(List<SearchResultItem> list, int which) {
		
		switch (which) {
		case Constants.SCJY:
			for (int i = 0; i < list.size(); i++) {
				list.get(i).flag = which;
				 mSearchResults1.add(list.get(i));
				
//				Log.v("LIST_ITEM", list.get(i).name);
				
			}

			break;
		case Constants.TK:
			for (int i = 0; i < list.size(); i++) {
				list.get(i).flag = which;
				mSearchResults1.add(list.get(i));
				
//				Log.v("LIST_ITEM", list.get(i).name);
				this.notifyDataSetChanged();
			}

			break;
		case Constants.TKCN:
			for (int i = 0; i < list.size(); i++) {
				list.get(i).flag = which;
				mSearchResults1.add(list.get(i));
				
//				Log.v("LIST_ITEM", list.get(i).name);
				
			}
			break;
		case Constants.BTDIGG:
			for (int i = 0; i < list.size(); i++) {
				list.get(i).flag = which;
				mSearchResults1.add(list.get(i));
				
//				Log.v("LIST_ITEM", list.get(i).name);
				
			}
			break;
		default:
			break;
		}
		this.notifyDataSetChanged();
		
	}

	public void setSearchResults(List<SearchResultItem> newList) {
		mSearchResults1 = newList;
		this.notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return mSearchResults1.size();
	}

	@Override
	public Object getItem(int position) {
		return mSearchResults1.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder = null;
		if (null == convertView) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.image_text_item, parent, false);
			holder = new ViewHolder();
			convertView.setTag(holder);
			holder.tvName = (TextView) convertView.findViewById(R.id.label);
			holder.imgTitle = (ImageView) convertView.findViewById(R.id.image);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		// 从哪里搜索的就替换上谁的图标
		if (mSearchResults1.get(position).flag == Constants.SCJY) {
			holder.imgTitle.setImageResource(R.drawable.sc);
		}
		if (mSearchResults1.get(position).flag == Constants.TK) {
			holder.imgTitle.setImageResource(R.drawable.kitty);
		}
		if (mSearchResults1.get(position).flag == Constants.TKCN) {
			holder.imgTitle.setImageResource(R.drawable.kitty);
		}
		if (mSearchResults1.get(position).flag == Constants.BTDIGG) {
			holder.imgTitle.setImageResource(R.drawable.btdigg);
		}
		if (null != holder.tvName) {
			String label = mSearchResults1.get(position).label;
			holder.tvName.setText(label);
			// holder.tvName.setCompoundDrawablesWithIntrinsicBounds(
			// getResourceByLabel(label), 0, 0, 0);
		}
		return convertView;
	}

	public final class ViewHolder {
		TextView tvName;
		ImageView imgTitle;
	}

	private int getResourceByLabel(String label) {
		/*
		 * String labelLowered = label.toLowerCase(Locale.CHINA); if
		 * (labelLowered.contains("mkv")) { return R.drawable.mkv; } else if
		 * (labelLowered.contains("mp4")) { return R.drawable.mp4; } else if
		 * (labelLowered.contains("avi")) { return R.drawable.avi; } else if
		 * (labelLowered.contains("rmvb") || labelLowered.contains("rm")) {
		 * return R.drawable.rm; }
		 */
		return 0;
	}

}
